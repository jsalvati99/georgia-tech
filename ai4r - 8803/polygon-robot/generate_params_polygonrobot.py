#Used to generate random parameters for PolygonRobot testing suite.
#
import random
import math
import pprint

PI = math.pi

def random_test_case(i, random_move=20):

    # Using OrderedDict here to preserve print order of keys
    return { 'test_case': i,
             'target_x': random.uniform(-20,20),
             'target_y': random.uniform(-20,20),
             'target_heading': random.uniform(0,2*PI) - PI, #Range of -pi,pi

             # target_period was previously random,
             # but instead cycle through 3-12,
             # randomly negating 50% of the time.
             'target_period': ((i % 10) + 3) * (1 - 2*random.randint(0,1)),
             'target_speed': random.uniform(1,5),
             'target_line_length': random.randint(1,16),
             'hunter_x': random.uniform(-20,20),
             'hunter_y': random.uniform(-20,20),
             'hunter_heading': random.uniform(0,2*PI) - PI,  #Range of -pi,pi
             'random_move': random_move }

def print_test_cases(n):

    cases = [None]  # placeholder so that we can index from 1
    for i in range(1,n+1):
        cases.append( random_test_case(i) )
    print "GLOBAL_PARAMETERS=" + pprint.pformat(cases)

if __name__ == '__main__':
    print_test_cases(10)
