# These import steps give you access to libraries which you may (or may
# not) want to use.
from math import *
from robot import *
from matrix import *
import random
from copy import deepcopy
import numpy as np
import scipy
import time

'''
test = {'test_case': 1,
        'target_x': 9.84595717195,
        'target_y': -3.82584680823,
        'target_heading': 1.95598927002,
        'target_period': -6,
        'target_speed': 2.23288537085,
        'target_line_length': 12,
        'hunter_x': -18.9289073476,
        'hunter_y': 18.7870153895,
        'hunter_heading': -1.94407132569,
        'random_move': 20}
'''
test = {'test_case': 1,
        'target_x': -8.23729263767,
        'target_y': 0.167449172934,
        'target_heading': -2.90891604491,
        'target_period': -8,
        'target_speed': 2.86280919028,
        'target_line_length': 5,
        'hunter_x': -1.26626321675,
        'hunter_y': 10.2766202621,
        'hunter_heading': -2.63089786461,
        'random_move': 20}



defaults = {'tolerance_ratio' : 0.02,
        	'part' : 2,
        	'max_steps' : 1000,
        	'noise_ratio' : 0.05,
            'speed_ratio' : 0.99}


def distance(p, q):
    """Calculate the distance between two points.

    Args:
        p(tuple): point 1.
        q(tuple): point 2.

    Returns:
        distance between points.
    """
    x1, y1 = p
    x2, y2 = q

    dx = x2 - x1
    dy = y2 - y1

    return sqrt(dx**2 + dy**2)





'''
def particle_filter(measurement, OTHER):

	#modified P6.py solution for problem set 3
	p = []
	N = 1000
    for i in range(N):
        r = robot()
        r.set_noise(bearing_noise, steering_noise, distance_noise)
        p.append(r)

    # --------
    #
    # Update particles
    #     

    for t in range(len(motions)):
    
        # motion update (prediction)
        p2 = []
        for i in range(N):
            p2.append(p[i].move(motions[t]))
        p = p2

        # measurement update
        w = []
        for i in range(N):
            w.append(p[i].measurement_prob(measurements[t]))

        # resampling
        p3 = []
        index = int(random.random() * N)
        beta = 0.0
        mw = max(w)
        for i in range(N):
            beta += random.random() * 2.0 * mw
            while beta > w[index]:
                beta -= w[index]
                index = (index + 1) % N
            p3.append(p[index])
        p = p3

	xy_estimate = measurement

	return xy_estimate, OTHER
'''
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


def polygon_detector(points):
	#given a list of points 
	#return a line length, an edge count, and an angle that represents the polygon
	#although this function handles it, dont bother calling with less than 3 points
    lastpointx = None
    lastpointy = None
    deltaxl = []
    deltayl = []
    edge_count = 0
    max_edge_count = 0
    line_length = 0  #in delta x y
    turns = 0
    turn_angle = 0
    point_count = 0
    next_turn = None

	#look for turns, once you hit 2, pop out of the for loop
    for i in range(len(points)):

      point_count += 1

      if lastpointy == None:
	  	#deal with the first
	  	lastpointx = points[i][0]
	  	lastpointy = points[i][1]
      else:
        deltaxl.append(points[i][0]-lastpointx)
        deltayl.append(points[i][1]-lastpointy)

        if len(deltayl)>1:
          if ((abs(deltayl[-1] - deltayl[-2]) >= 2) or (abs(deltaxl[-1] - deltaxl[-2]) >= 2)):
		  	#polygon turn
            turns +=1
            max_edge_count = edge_count
            if(next_turn == None):
                next_turn = edge_count

            edge_count = 0
            turn_angle = (atan2(deltayl[-1],deltaxl[-1]))-(atan2(deltayl[-2],deltaxl[-2]))
		  	#hypo_distance(sqrt(((points[i][0])-(points[i-2][0]))**2
		  	#					(points[i][1])-(points[i-2][1]))**2)
          else:
		  	#line
            edge_count += 1

            line_length = sqrt(deltaxl[-1]*deltaxl[-1]+deltayl[-1]*deltayl[-1])

      lastpointx = points[i][0]
      lastpointy = points[i][1]
   
	  
      if(turns == 2):

        if(max_edge_count == 0):
            max_edge_count +=1
            line_length = sqrt((deltaxl[-1]*deltaxl[-1])+(deltayl[-1]*deltayl[-1]))
	  	#we've got what we need, lets break out of this joint
        break


    return line_length,turn_angle,max_edge_count,next_turn




# This is the function you have to write. The argument 'measurement' is a 
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be 
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def geometry_method(measurement, OTHER = None):
    if(OTHER == None):
        OTHER = []

    if len(OTHER) > 2:

        if(len(OTHER) > 40):
            line_length,turn_angle,max_edge_count,next_turn = polygon_detector(OTHER[-40:])
        else:
            line_length,turn_angle,max_edge_count,next_turn = polygon_detector(OTHER)

        deltax = measurement[0] - OTHER[-1][0]
        deltay = measurement[1] - OTHER[-1][1]

        heading = atan2(deltay,deltax)

        if(max_edge_count == 1 or (next_turn == 0)):
            xy_estimate = (measurement[0] + line_length*cos(turn_angle+heading),
                           measurement[1] + line_length*sin(turn_angle+heading))
        else:
            xy_estimate = (measurement[0] + deltax,measurement[1] + deltay)

    else:
        xy_estimate = measurement


    OTHER.append(measurement)

    # You must return xy_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.
    return xy_estimate, OTHER


initial_xy = [0., 0.]
#initial matrixs
F = matrix([[1.,0,   1,0],
            [0,1.,   0,1],
            [0,0,   1.,0],
            [0,0,   0,1.]]) # next state function: generalize the 2d version to 4d

H = matrix([[1.,0.,0.,0.],
            [0.,1.,0.,0.]]) # measurement function: reflect the fact that we observe x and y but not the two velocities
R = matrix([[0.001,0],[0,0.001]]) # measurement uncertainty: use 2x2 matrix with 0.1 as main diagonal
I = matrix([[1.,0,0,0],
            [0,1.,0,0],
            [0,0,1.,0],
            [0,0,0,1.]]) # 4d identity matrix
x = matrix([[initial_xy[0]], [initial_xy[1]], [0.], [0.]])
u = matrix([[0.], [0.], [0.], [0.]])
Q =  matrix([[.5,0,   0,0],
            [0,.5,   0,0],
            [0,0,.5,0],
            [0,0,   0,.5]])

#starting from ps2
def kalhman_filter(x, P, measurement, turn_theta = None):

    #print measurement
    z = [measurement[0],measurement[1]]
    #print z

    if(turn_theta == None):
        F = matrix([[1.,0,   1,0],
            [0,1.,   0,1],
            [0,0,   1.,0],
            [0,0,   0,1.]]) # next state function: generalize the 2d version to 4d
    else:
        F = matrix([[cos(turn_theta),0,   1,0],
            [0,sin(turn_theta),   0,1],
            [0,0,   1.,0],
            [0,0,   0,1.]]) # next state function: generalize the 2d version to 4d

    # prediction
    x = (F * x) + u
    P = (F * P * F.transpose()) + Q

    
    # measurement update
    Z = matrix([z])

    y = Z.transpose() - (H * x)
    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    x = x + (K * y)
    P = (I - (K * H)) * P 

    return x,P



def kalman_method(measurement, OTHER = None):

    if(OTHER == None):

        P = matrix([[1000.,0,   0,0],
            [0,1000.,   0,0],
            [0,0,1000.,0],
            [0,0,   0,1000.]])
        x = matrix([[measurement[0]], [measurement[1]], [0.], [0.]])

        OTHER = [[],[],[]]
        l = []
 
    else:
        l = OTHER[0]
        x = OTHER[1]
        P = OTHER[2]

    l.append(measurement)

    if len(l) > 2:

        line_length,turn_angle,max_edge_count = polygon_detector(l)

        deltax = measurement[0] - l[-1][0]
        deltay = measurement[1] - l[-1][1]

        heading = atan2(deltay,deltax)


        if(max_edge_count == 1):

            x,P = kalhman_filter(x,P,measurement,turn_angle+heading)

            xy_estimate = (x[0][0]+x[2][0],x[1][0]+(x[1][0]-x[3][0]))
            #xy_estimate = (x[0][0]+x[2][0],x[1][0]+x[3][0])
        else:
            x,P = kalhman_filter(x,P,measurement)
            xy_estimate = (x[0][0]+x[2][0],x[1][0]+(x[1][0]-x[3][0]))
            #xy_estimate = (x[0][0]+x[2][0],x[1][0]+x[3][0])

    else:
        xy_estimate = measurement
        x,P = kalhman_filter(x,P,measurement)

    OTHER = (l,x,P)

    # You must return xy_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.
    return xy_estimate, OTHER


# This is the function you have to write. The argument 'measurement' is a 
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be 
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    xy_estimate, OTHER = geometry_method(measurement, OTHER)
    #xy_estimate, OTHER = kalman_method(measurement, OTHER)

    # You must return xy_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.
    return xy_estimate, OTHER

    # This function will be called after each time the target moves. 
    # The OTHER variable is a place for you to store any historical 
    # information about the progress of the hunt (or maybe some 
    # localization information). Your must return a tuple of three 
    # values: turning, distance, OTHER

def truncate_angle(a):
    """This maps all angles to a domain of [-pi, pi]"""
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi

def get_heading(hunter_position, target_position):
    """Returns the angle, in radians, between the target and hunter positions"""
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading

'''
def line_distance(line_length,turn_angle,max_edge_count,line_count,target_position,current_position):
    #given, attributes of a polygon shape + the current position on the polygon
    #return, the distance to target position along the polygon (as opposed to crow distance)
'''

stutter = False
lastPosition = None
distances = []

def next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER = None):


    xy_estimate, OTHER = estimate_next_pos(target_measurement, OTHER)

    distance_to_target = distance_between(hunter_position,xy_estimate)
    nsteps = distance_to_target/max_distance

    if(len(OTHER) > 40):
        (line_length,turn_angle,max_edge_count,next_turn) = polygon_detector(OTHER[-40:])
    else:
        (line_length,turn_angle,max_edge_count,next_turn) = polygon_detector(OTHER)

    OTHER2 = deepcopy(OTHER)


    if(len(distances)>3):
        if((distances[-1:] >= distance_to_target-.05) and (distance_to_target < 5)):
            #print "stutttererrrr!"
            stutter = True
        else:
            stutter = False
    
    if(nsteps > 4 and nsteps < 20):
        #cant catch him on the line, go forward a bunch and try to cut the corner
        #aim two spots ahead until we are within striking distance
        for i in range(int(ceil(nsteps+4))):

            xy_estimate, OTHER2 = estimate_next_pos(xy_estimate, OTHER2)
    elif (nsteps >0.99 and nsteps <4):
        #are we hunting or chasing? 
        if(False):
            xy_estimate = hunter_position
        else:
            for i in range(int(ceil(nsteps))):
                xy_estimate, OTHER2 = estimate_next_pos(xy_estimate, OTHER2)

    distances.append(distance_to_target)
    #print "final goal = ",xy_estimate

    '''
    if(nsteps > 0.99 and nsteps < 20):
        #cant catch him on the line, go forward a bunch and try to cut the corner
        #aim two spots ahead until we are within striking distance
        while(distance_between(hunter_position,xy_estimate)
    '''



    '''
    i = 0
    if(len(OTHER[0]) > 12):
        while (i < 30) and (distance_to_target < line_distance)
    '''


    heading_to_target = get_heading(hunter_position, xy_estimate)
    heading_difference = heading_to_target - hunter_heading
    turning =  heading_difference # turn towards the target
    distance = distance_between(hunter_position,xy_estimate) # full speed ahead!



    return turning, distance, OTHER


def simulate_with_hunter():
    """Run simulation to locate lost bot and catch with hunter.

    Args:
        next_move(func): Student submission function for hunters next move.
        params(dict): Test parameters.

    Raises:
        Exception if error running submission.
    """

    target = robot(test['target_x'],
                         test['target_y'],
                         test['target_heading'],
                         2.0 * pi / test['target_period'],
                         test['target_speed'],
                         test['target_line_length'],
                         test['random_move'])
    target.set_noise(0.0,
                     0.0,
                     defaults['noise_ratio'] * test['target_speed'])

    hunter = robot(test['hunter_x'],
                         test['hunter_y'],
                         test['hunter_heading'])

    tolerance = defaults['tolerance_ratio'] * target.distance
    max_speed = defaults['speed_ratio'] * test['target_speed']
    other_info = None
    steps = 0

    #random.seed(GLOBAL_SEEDS[test['part']])



    import turtle
    window = turtle.Screen()
    window.bgcolor('white')
    chaser_robot = turtle.Turtle()
    chaser_robot.shape('arrow')
    chaser_robot.color('blue')
    chaser_robot.resizemode('user')
    chaser_robot.shapesize(0.3, 0.3, 0.3)
    broken_robot = turtle.Turtle()
    broken_robot.shape('turtle')
    broken_robot.color('green')
    broken_robot.resizemode('user')
    broken_robot.shapesize(0.3, 0.3, 0.3)
    size_multiplier = 10.0 #change Size of animation
    chaser_robot.hideturtle()
    chaser_robot.penup()
    chaser_robot.goto(hunter.x*size_multiplier, hunter.y*size_multiplier-100)
    chaser_robot.showturtle()
    broken_robot.hideturtle()
    broken_robot.penup()
    broken_robot.goto(target.x*size_multiplier, target.y*size_multiplier-100)
    broken_robot.showturtle()
    measuredbroken_robot = turtle.Turtle()
    measuredbroken_robot.shape('circle')
    measuredbroken_robot.color('red')
    measuredbroken_robot.penup()
    measuredbroken_robot.resizemode('user')
    measuredbroken_robot.shapesize(0.1, 0.1, 0.1)
    broken_robot.pendown()
    chaser_robot.pendown()




    while steps < defaults['max_steps']:
        hunter_pos = (hunter.x, hunter.y)
        target_pos = (target.x, target.y)

        separation = distance(hunter_pos, target_pos)
        if separation < tolerance:
            print "caught the robot!"
            return

        target_meas = target.sense()

        turn, dist, other_info = next_move(hunter_pos, hunter.heading, target_meas, max_speed, other_info)


        dist = min(dist, max_speed)
        dist = max(dist, 0)
        turn = truncate_angle(turn)

        hunter.move(turn, dist)
        target.move_in_polygon()


        measuredbroken_robot.setheading(target.heading*180/pi)
        measuredbroken_robot.goto(target_meas[0]*size_multiplier, target_meas[1]*size_multiplier-100)
        measuredbroken_robot.stamp()
        broken_robot.setheading(target.heading*180/pi)
        broken_robot.goto(target.x*size_multiplier, target.y*size_multiplier-100)
        chaser_robot.setheading(hunter.heading*180/pi)
        chaser_robot.goto(hunter.x*size_multiplier, hunter.y*size_multiplier-100)



        steps += 1

        #self.robot_found.put(False)
        #self.robot_steps.put(steps)





#simulate_with_hunter()




'''
    localized = False
    distance_tolerance = 0.01 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    #For Visualization
    import turtle    #You need to run this locally to use the turtle module
    window = turtle.Screen()
    window.bgcolor('white')
    size_multiplier= 25.0  #change Size of animation
    broken_robot = turtle.Turtle()
    broken_robot.shape('turtle')
    broken_robot.color('green')
    broken_robot.resizemode('user')
    broken_robot.shapesize(0.1, 0.1, 0.1)
    measured_broken_robot = turtle.Turtle()
    measured_broken_robot.shape('circle')
    measured_broken_robot.color('red')
    measured_broken_robot.resizemode('user')
    measured_broken_robot.shapesize(0.1, 0.1, 0.1)
    prediction = turtle.Turtle()
    prediction.shape('arrow')
    prediction.color('blue')
    prediction.resizemode('user')
    prediction.shapesize(0.1, 0.1, 0.1)
    prediction.penup()
    broken_robot.penup()
    measured_broken_robot.penup()
    #End of Visualization
    while not localized and ctr <= 10:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        print "error = ",error
        #if error <= distance_tolerance:
        #    print "You got it right! It took you ", ctr, " steps to localize."
        #    localized = True
        if ctr == 10:
            print "Sorry, it took you too many steps to localize the target."
        #More Visualization
        measured_broken_robot.setheading(target_bot.heading*180/pi)
        measured_broken_robot.goto(measurement[0]*size_multiplier, measurement[1]*size_multiplier-200)
        measured_broken_robot.stamp()
        broken_robot.setheading(target_bot.heading*180/pi)
        broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-200)
        broken_robot.stamp()
        prediction.setheading(target_bot.heading*180/pi)
        prediction.goto(position_guess[0]*size_multiplier, position_guess[1]*size_multiplier-200)
        prediction.stamp()
        time.sleep(1)

        #End of Visualization
    return localized


'''