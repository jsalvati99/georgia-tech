# These import steps give you access to libraries which you may (or may
# not) want to use.
from math import *
from robot import *
from matrix import *
import random
from copy import deepcopy
import numpy as np
import scipy
import time

test1 = {'test_case': 1,
        'target_x': 9.84595717195,
        'target_y': -3.82584680823,
        'target_heading': 1.95598927002,
        'target_period': -6,
        'target_speed': 2.23288537085,
        'target_line_length': 12,
        'hunter_x': -18.9289073476,
        'hunter_y': 18.7870153895,
        'hunter_heading': -1.94407132569,
        'random_move': 20}


test2 = {'test_case': 6,
                              'target_x': 19.8033444747,
                      'target_y': 15.8607456499,
                      'target_heading': 2.91674681677,
                      'target_period': 10,
                      'target_speed': 4.11574616586,
                      'target_line_length': 1,
                      'hunter_x': -13.483627167,
                      'hunter_y': 7.60284054436,
                      'hunter_heading': 2.45511184918,
                              'random_move': 20}

test = {'test_case': 6,
                              'target_x': 1,
                      'target_y': 1,
                      'target_heading': 0,
                      'target_period': 10,
                      'target_speed': 1,
                      'target_line_length': 1,
                      'hunter_x': 1,
                      'hunter_y': 1,
                      'hunter_heading': 1,
                              'random_move': 20}

defaults = {'tolerance_ratio' : 0.02,
        	'part' : 1,
        	'max_steps' : 50,
        	'noise_ratio' : 0.00}


def distance(p, q):
    """Calculate the distance between two points.

    Args:
        p(tuple): point 1.
        q(tuple): point 2.

    Returns:
        distance between points.
    """
    x1, y1 = p
    x2, y2 = q

    dx = x2 - x1
    dy = y2 - y1

    return sqrt(dx**2 + dy**2)





'''
def particle_filter(measurement, OTHER):

	#modified P6.py solution for problem set 3
	p = []
	N = 1000
    for i in range(N):
        r = robot()
        r.set_noise(bearing_noise, steering_noise, distance_noise)
        p.append(r)

    # --------
    #
    # Update particles
    #     

    for t in range(len(motions)):
    
        # motion update (prediction)
        p2 = []
        for i in range(N):
            p2.append(p[i].move(motions[t]))
        p = p2

        # measurement update
        w = []
        for i in range(N):
            w.append(p[i].measurement_prob(measurements[t]))

        # resampling
        p3 = []
        index = int(random.random() * N)
        beta = 0.0
        mw = max(w)
        for i in range(N):
            beta += random.random() * 2.0 * mw
            while beta > w[index]:
                beta -= w[index]
                index = (index + 1) % N
            p3.append(p[index])
        p = p3

	xy_estimate = measurement

	return xy_estimate, OTHER
'''

def polygon_detector(points):
	#given a list of points 
	#return a line length, an edge count, and an angle that represents the polygon
	#although this function handles it, dont bother calling with less than 3 points

	lastpointx = None
	lastpointy = None

	deltaxl = []
	deltayl = []

	edge_count = 0 
	max_edge_count = 0
	line_length = 0  #in delta x y
	turns = 0
	turn_angle = 0

	#look for turns, once you hit 2, pop out of the for loop
	for i in range(len(points)):

	  if lastpointy == None:
	  	#deal with the first
	  	lastpointx = points[i][0]
	  	lastpointy = points[i][1]
	  else:
	  	deltaxl.append(points[i][0]-lastpointx)
		deltayl.append(points[i][1]-lastpointy)

		if len(deltayl)>2:
		  if (deltayl[-1] != deltayl[-2]) or (deltaxl[-1] != deltaxl[-2]):
		  	#polygon turn
		  	turns +=1
		  	max_edge_count = edge_count
		  	edge_count = 0
		  	turn_angle = (atan2(deltayl[-1],deltaxl[-1]))-(atan2(deltayl[-2],deltaxl[-2]))
		  	#hypo_distance(sqrt(((points[i][0])-(points[i-2][0]))**2
		  	#					(points[i][1])-(points[i-2][1]))**2)
		  else:
		  	#line
		  	edge_count += 1

			line_length = sqrt(deltaxl[-1]*deltaxl[-1]+deltayl[-1]*deltayl[-1])

	  lastpointx = points[i][0]
	  lastpointy = points[i][1]

	  
	  if(turns == 2):

	  	if(max_edge_count == 0):
	  		max_edge_count +=1

	  		if(deltaxl[-1] == 0):
		  		line_length = deltayl[-1]
		  	elif (deltayl[-1] == 0):
		  		line_length = deltaxl[-1]
		  	else:
				line_length = sqrt((deltaxl[-1]*deltaxl[-1])+(deltayl[-1]*deltayl[-1]))
	  	#we've got what we need, lets break out of this joint
	  	break
	'''  
	if(max_edge_count == 0):
		max_edge_count +=1

		if(deltaxl[-1] == 0):
  			line_length = deltayl[-1]
  		elif (deltayl[-1] == 0):
  			line_length = deltaxl[-1]
  		else:
			line_length = sqrt((deltaxl[-1]*deltaxl[-1])+(deltayl[-1]*deltayl[-1]))
	'''



	return line_length,turn_angle,max_edge_count




# This is the function you have to write. The argument 'measurement' is a 
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be 
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    if(OTHER == None):
    	OTHER = []

    if len(OTHER) > 2:

    	line_length,turn_angle,max_edge_count = polygon_detector(OTHER)

    	deltax = measurement[0] - OTHER[-1][0]
    	deltay = measurement[1] - OTHER[-1][1]

    	heading = atan2(deltay,deltax)

    	if(max_edge_count == 1):
    		xy_estimate = (measurement[0] + line_length*cos(turn_angle+heading),
    					   measurement[1] + line_length*sin(turn_angle+heading))
    	else:
    		xy_estimate = (measurement[0] + deltax,measurement[1] + deltay)

    else:
    	xy_estimate = measurement


    OTHER.append(measurement)

    # You must return xy_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.
    return xy_estimate, OTHER


def simulate_without_hunter():
    """Run simulation only to locate lost bot.

    Args:
        estimate_next_pos(func): Student submission function to estimate next robot position.
        params(dict): Test parameters.

    Raises:
        Exception if error running submission.
    """
    target = robot(test['target_x'],
                         test['target_y'],
                         test['target_heading'],
                         2.0 * pi / test['target_period'],
                         test['target_speed'],
                         test['target_line_length'],
                         test['random_move'])
    target.set_noise(0.0,
                     0.0,
                     defaults['noise_ratio'] * test['target_speed'])

    tolerance = defaults['tolerance_ratio'] * target.distance
    other_info = None
    steps = 0

    #random.seed(GLOBAL_SEEDS[params['part']])

    try:

    	import turtle    
    	window = turtle.Screen()
    	window.bgcolor('white')
    	size_multiplier= 25.0
    	broken_robot = turtle.Turtle()
    	broken_robot.shape('turtle')
    	broken_robot.color('green')
    	broken_robot.resizemode('user')
    	broken_robot.shapesize(0.1, 0.1, 0.1)
    	measured_broken_robot = turtle.Turtle()
    	measured_broken_robot.shape('circle')
    	measured_broken_robot.color('red')
    	measured_broken_robot.resizemode('user')
    	measured_broken_robot.shapesize(0.1, 0.1, 0.1)
    	prediction = turtle.Turtle()
    	prediction.shape('arrow')
    	prediction.color('blue')
    	prediction.resizemode('user')
    	prediction.shapesize(0.1, 0.1, 0.1)
    	prediction.penup()
    	broken_robot.penup()
    	measured_broken_robot.penup()



        while steps < defaults['max_steps']:
            target_meas = target.sense()

            estimate, other_info = estimate_next_pos(target_meas, other_info)

            target.move_in_polygon()
            target_pos = (target.x, target.y)

            separation = distance(estimate, target_pos)

            measured_broken_robot.setheading(target.heading*180/pi)
            measured_broken_robot.goto(target_meas[0]*size_multiplier, target_meas[1]*size_multiplier-200)
            measured_broken_robot.stamp()
            broken_robot.setheading(target.heading*180/pi)
            broken_robot.goto(target.x*size_multiplier, target.y*size_multiplier-200)
            broken_robot.stamp()
            prediction.setheading(target.heading*180/pi)
            prediction.goto(estimate[0]*size_multiplier, estimate[1]*size_multiplier-200)
            prediction.stamp()	

            if separation < tolerance:
                print "hit the target!"
                return

            steps += 1
            time.sleep(1)

        print "failed to get the target"
        return

    except Exception,e:
        print e



#simulate_without_hunter()




'''
    localized = False
    distance_tolerance = 0.01 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    #For Visualization
    import turtle    #You need to run this locally to use the turtle module
    window = turtle.Screen()
    window.bgcolor('white')
    size_multiplier= 25.0  #change Size of animation
    broken_robot = turtle.Turtle()
    broken_robot.shape('turtle')
    broken_robot.color('green')
    broken_robot.resizemode('user')
    broken_robot.shapesize(0.1, 0.1, 0.1)
    measured_broken_robot = turtle.Turtle()
    measured_broken_robot.shape('circle')
    measured_broken_robot.color('red')
    measured_broken_robot.resizemode('user')
    measured_broken_robot.shapesize(0.1, 0.1, 0.1)
    prediction = turtle.Turtle()
    prediction.shape('arrow')
    prediction.color('blue')
    prediction.resizemode('user')
    prediction.shapesize(0.1, 0.1, 0.1)
    prediction.penup()
    broken_robot.penup()
    measured_broken_robot.penup()
    #End of Visualization
    while not localized and ctr <= 10:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        print "error = ",error
        #if error <= distance_tolerance:
        #    print "You got it right! It took you ", ctr, " steps to localize."
        #    localized = True
        if ctr == 10:
            print "Sorry, it took you too many steps to localize the target."
        #More Visualization
        measured_broken_robot.setheading(target_bot.heading*180/pi)
        measured_broken_robot.goto(measurement[0]*size_multiplier, measurement[1]*size_multiplier-200)
        measured_broken_robot.stamp()
        broken_robot.setheading(target_bot.heading*180/pi)
        broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-200)
        broken_robot.stamp()
        prediction.setheading(target_bot.heading*180/pi)
        prediction.goto(position_guess[0]*size_multiplier, position_guess[1]*size_multiplier-200)
        prediction.stamp()
        time.sleep(1)

        #End of Visualization
    return localized


'''