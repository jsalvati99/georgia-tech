# Optimize your PID parameters here:
pressure_tau_p = 2.
pressure_tau_d = 3.


rocket_tau_p = 18.
rocket_tau_i = 29.
rocket_tau_d = 20.

def pressure_pd_solution(delta_t, current_pressure, target_pressure, data):
    """Student solution to maintain LOX pressure to the turbopump at a level of 100.

    Args:
        delta_t (float): Time step length.
        current_pressure (float): Current pressure level of the turbopump.
        target_pressure (float): Target pressure level of the turbopump.
        data (dict): Data passed through out run.  Additional data can be added and existing values modified.
            'ErrorP': Proportional error.  Initialized to 0.0
            'ErrorD': Derivative error.  Initialized to 0.0
    """
    if 'lastError' in data:
        lastE = data['lastError']
    else:
        lastE = current_pressure

    # TODO: remove naive solution
    error = target_pressure - current_pressure
    errp = pressure_tau_p * error
    errd = ((error-lastE)/delta_t)*pressure_tau_d
    adjust_pressure = errp + errd
    data['lastError'] = error
    data['ErrorP'] = errp
    data['ErrorD'] = errd


    # TODO: implement PD solution here

    return adjust_pressure, data


def rocket_pid_solution(delta_t, current_velocity, optimal_velocity, data):
    """Student solution for maintaining rocket throttle through out the launch based on an optimal flight path

    Args:
        delta_t (float): Time step length.
        current_velocity (float): Current velocity of rocket.
        optimal_velocity (float): Optimal velocity of rocket.
        data (dict): Data passed through out run.  Additional data can be added and existing values modified.
            'ErrorP': Proportional error.  Initialized to 0.0
            'ErrorI': Integral error.  Initialized to 0.0
            'ErrorD': Derivative error.  Initialized to 0.0

    Returns:
        Throttle to set, data dictionary to be passed through run.
    """
    '''
    rocket_tau_p = params[0]
    rocket_tau_d = params[1]
    rocket_tau_i = params[2]
'''

    if 'lastError' in data:
        lastE = data['lastError']
    else:
        lastE = current_velocity

    if 'errorSum' in data:
        errS = data['lastError']
    else:
        errS = 0

    error = optimal_velocity - current_velocity
    errp = rocket_tau_p * error
    errd = ((error-lastE)/delta_t)*rocket_tau_d
    erri = rocket_tau_i * errS
    throttle = errp + errd + erri

    errS += error
    data['lastError'] = error
    data['errorSum']  = errS
    data['ErrorP'] = errp
    data['ErrorD'] = errd
    data['ErrorI'] = erri

    return throttle, data



