# ----------
# Part Three
#
# Now you'll actually track down and recover the runaway Traxbot. 
# In this step, your speed will be about twice as fast the runaway bot,
# which means that your bot's distance parameter will be about twice that
# of the runaway. You can move less than this parameter if you'd 
# like to slow down your bot near the end of the chase. 
#
# ----------
# YOUR JOB
#
# Complete the next_move function. This function will give you access to 
# the position and heading of your bot (the hunter); the most recent 
# measurement received from the runaway bot (the target), the max distance
# your bot can move in a given timestep, and another variable, called 
# OTHER, which you can use to keep track of information.
# 
# Your function will return the amount you want your bot to turn, the 
# distance you want your bot to move, and the OTHER variable, with any
# information you want to keep track of.
# 
# ----------
# GRADING
# 
# We will make repeated calls to your next_move function. After
# each call, we will move the hunter bot according to your instructions
# and compare its position to the target bot's true position
# As soon as the hunter is within 0.01 stepsizes of the target,
# you will be marked correct and we will tell you how many steps it took
# before your function successfully located the target bot. 
#
# As an added challenge, try to get to the target bot as quickly as 
# possible. 

from robot import *
from math import *
from matrix import *
import random
import numpy
from scipy import optimize

def angle_trunc(a):
    return ((a + pi) % (pi * 2)) - pi

#adaptation of example 2 from https://www.programcreek.com/python/example/52479/scipy.optimize.leastsq
def _optLeastSqCircle(x,y):
    def calcR(x,y, xc, yc):
      '''
        Calculate distance of each point from the center (xc, yc) .
      '''
      return numpy.sqrt((x-xc)**2 + (y-yc)**2)

    def f(c, x, y):
      '''
        Calculate the algebraic distance between the data points and the mean 
        circle centered at c = (xc, yc).
      '''
      Ri = calcR(x, y, *c)
      return Ri - Ri.mean()   

    x_m = numpy.mean(x)
    y_m = numpy.mean(y)
    centre_estimate = x_m, y_m
    centre, ier = optimize.leastsq(f, centre_estimate, args=(x,y))
    xc, yc = centre
    Ri = calcR(x, y, *centre)
    R = Ri.mean()

    residuals = numpy.sqrt((Ri - R)**2)
    
    return xc, yc, R, residuals 


initial_xy = [0., 0.]
#initial matrixs
F = matrix([[1.,0,   1,0],
            [0,1.,   0,1],
            [0,0,   1.,0],
            [0,0,   0,1.]]) # next state function: generalize the 2d version to 4d
H = matrix([[1.,0.,0.,0.],
            [0.,1.,0.,0.]]) # measurement function: reflect the fact that we observe x and y but not the two velocities
R = matrix([[0.1,0],[0,0.1]]) # measurement uncertainty: use 2x2 matrix with 0.1 as main diagonal
I = matrix([[1.,0,0,0],
            [0,1.,0,0],
            [0,0,1.,0],
            [0,0,0,1.]]) # 4d identity matrix
x = matrix([[initial_xy[0]], [initial_xy[1]], [0.], [0.]])
u = matrix([[0.], [0.], [0.], [0.]])
Q =  matrix([[100.,0,   0,0],
            [0,100.,   0,0],
            [0,0,100.,0],
            [0,0,   0,100.]])

#starting from ps2
def kalhman_filter(x, P, measurement):

    #print measurement
    z = [measurement[0],measurement[1]]
    #print z
    # prediction
    x = (F * x) + u
    P = (F * P * F.transpose()) + Q

    
    # measurement update
    Z = matrix([z])

    y = Z.transpose() - (H * x)
    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    x = x + (K * y)
    P = (I - (K * H)) * P 

    return x,P


def assisted_kal(measurement, OTHER):

    if(OTHER == None):
        P = matrix([[1.,0,   0,0],
            [0,1.,   0,0],
            [0,0,1.,0],
            [0,0,   0,1.]])
        x = matrix([[initial_xy[0]], [initial_xy[1]], [0.], [0.]])
        OTHER = []
        l = [[],[]]
    else:
        x = OTHER[0]
        P = OTHER[1]
        l = OTHER[2]

    l[0].append(measurement[0])
    l[1].append(measurement[1])

    if(len(l[0]) > 5):
        centerx,centery,r,rs = _optLeastSqCircle(l[0],l[1])


        #print "circle center; "+str(centerx)+" ",centery

        deltax = measurement[0]-centerx
        deltay = measurement[1]-centery

        r0 = sqrt(deltax**2+deltay**2)
        rt0 = atan2(deltay,deltax)

        if(rt0<0):
            flipY = True
            rt0 = atan2(-deltay,deltax)
        else:
            flipY = False

    else:
        r0 = sqrt(measurement[0]**2+measurement[1]**2)
        rt0 = atan2(measurement[1],measurement[0])

    #rt0 = angle_trunc(rt0)
    x,P = kalhman_filter(x,P,(r0,rt0))
    OTHER = (x,P,l)

    r0 = x[0][0]
    rt0 = x[1][0]

    r1  = r0 + x[2][0]
    rt1 = rt0 + x[3][0]

    '''
    print "measurement : ",measurement
    print "rs : "+str(r0)+" ",rt0
    print "x : ",x
    '''
    if(len(l[0]) > 5):
        x1 = (r0*cos(rt0))+centerx
        y1 = (r0*sin(rt0))+centery

        x2 = (r1*cos(rt1))+centerx

        if(flipY):
            y2 = -(r1*sin(rt1))+centery
        else:
            y2 = (r1*sin(rt1))+centery
    else:
        x2 = r0*cos(rt0)
        y2 = r0*sin(rt0)

    xy_estimate = (x2,y2)

    # You must return xy_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.

    return xy_estimate, OTHER


def next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER = None):
    # This function will be called after each time the target moves. 

    # The OTHER variable is a place for you to store any historical information about
    # the progress of the hunt (or maybe some localization information). Your return format
    # must be as follows in order to be graded properly.

    xy_estimate, OTHER = assisted_kal(target_measurement, OTHER)

    heading_to_target = get_heading(hunter_position, xy_estimate)
    heading_difference = heading_to_target - hunter_heading
    turning =  heading_difference # turn towards the target
    distance = distance_between(hunter_position,xy_estimate) # full speed ahead!


    return turning, distance, OTHER

def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

'''
def demo_grading(hunter_bot, target_bot, next_move_fcn, OTHER = None):
    """Returns True if your next_move_fcn successfully guides the hunter_bot
    to the target_bot. This function is here to help you understand how we 
    will grade your submission."""
    max_distance = 1.94 * target_bot.distance # 1.94 is an example. It will change.
    separation_tolerance = 0.02 * target_bot.distance # hunter must be within 0.02 step size to catch target
    caught = False
    ctr = 0

    # We will use your next_move_fcn until we catch the target or time expires.
    while not caught and ctr < 1000:

        # Check to see if the hunter has caught the target.
        hunter_position = (hunter_bot.x, hunter_bot.y)
        target_position = (target_bot.x, target_bot.y)
        separation = distance_between(hunter_position, target_position)
        if separation < separation_tolerance:
            print "You got it right! It took you ", ctr, " steps to catch the target."
            caught = True

        # The target broadcasts its noisy measurement
        target_measurement = target_bot.sense()

        # This is where YOUR function will be called.
        turning, distance, OTHER = next_move_fcn(hunter_position, hunter_bot.heading, target_measurement, max_distance, OTHER)
        
        # Don't try to move faster than allowed!
        if distance > max_distance:
            distance = max_distance

        # We move the hunter according to your instructions
        hunter_bot.move(turning, distance)

        # The target continues its (nearly) circular motion.
        target_bot.move_in_circle()

        ctr += 1            
        if ctr >= 1000:
            print "It took too many steps to catch the target."
    return caught
'''

'''
def demo_grading(hunter_bot, target_bot, next_move_fcn, OTHER = None):
    """Returns True if your next_move_fcn successfully guides the hunter_bot
    to the target_bot. This function is here to help you understand how we 
    will grade your submission."""
    max_distance = 1.94 * target_bot.distance # 1.94 is an example. It will change.
    separation_tolerance = 0.02 * target_bot.distance # hunter must be within 0.02 step size to catch target
    caught = False
    ctr = 0
    #For Visualization
    import turtle
    window = turtle.Screen()
    window.bgcolor('white')
    chaser_robot = turtle.Turtle()
    chaser_robot.shape('arrow')
    chaser_robot.color('blue')
    chaser_robot.resizemode('user')
    chaser_robot.shapesize(0.3, 0.3, 0.3)
    broken_robot = turtle.Turtle()
    broken_robot.shape('turtle')
    broken_robot.color('green')
    broken_robot.resizemode('user')
    broken_robot.shapesize(0.3, 0.3, 0.3)
    size_multiplier = 15.0 #change Size of animation
    chaser_robot.hideturtle()
    chaser_robot.penup()
    chaser_robot.goto(hunter_bot.x*size_multiplier, hunter_bot.y*size_multiplier-100)
    chaser_robot.showturtle()
    broken_robot.hideturtle()
    broken_robot.penup()
    broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-100)
    broken_robot.showturtle()
    measuredbroken_robot = turtle.Turtle()
    measuredbroken_robot.shape('circle')
    measuredbroken_robot.color('red')
    measuredbroken_robot.penup()
    measuredbroken_robot.resizemode('user')
    measuredbroken_robot.shapesize(0.1, 0.1, 0.1)
    broken_robot.pendown()
    chaser_robot.pendown()
    #End of Visualization
    # We will use your next_move_fcn until we catch the target or time expires.
    while not caught and ctr < 1000:
        # Check to see if the hunter has caught the target.
        hunter_position = (hunter_bot.x, hunter_bot.y)
        target_position = (target_bot.x, target_bot.y)
        separation = distance_between(hunter_position, target_position)
        if separation < separation_tolerance:
            print "You got it right! It took you ", ctr, " steps to catch the target."
            caught = True

        # The target broadcasts its noisy measurement
        target_measurement = target_bot.sense()

        # This is where YOUR function will be called.
        turning, distance, OTHER = next_move_fcn(hunter_position, hunter_bot.heading, target_measurement, max_distance, OTHER)

        # Don't try to move faster than allowed!
        if distance > max_distance:
            distance = max_distance

        # We move the hunter according to your instructions
        hunter_bot.move(turning, distance)

        # The target continues its (nearly) circular motion.
        target_bot.move_in_circle()
        #Visualize it
        measuredbroken_robot.setheading(target_bot.heading*180/pi)
        measuredbroken_robot.goto(target_measurement[0]*size_multiplier, target_measurement[1]*size_multiplier-100)
        measuredbroken_robot.stamp()
        broken_robot.setheading(target_bot.heading*180/pi)
        broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-100)
        chaser_robot.setheading(hunter_bot.heading*180/pi)
        chaser_robot.goto(hunter_bot.x*size_multiplier, hunter_bot.y*size_multiplier-100)
        #End of visualization
        ctr += 1            
        if ctr >= 1000:
            print "It took too many steps to catch the target."
    return caught
'''



def angle_trunc(a):
    """This maps all angles to a domain of [-pi, pi]"""
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi

def get_heading(hunter_position, target_position):
    """Returns the angle, in radians, between the target and hunter positions"""
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading

def naive_next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER):
    """This strategy always tries to steer the hunter directly towards where the target last
    said it was and then moves forwards at full speed. This strategy also keeps track of all 
    the target measurements, hunter positions, and hunter headings over time, but it doesn't 
    do anything with that information."""
    if not OTHER: # first time calling this function, set up my OTHER variables.
        measurements = [target_measurement]
        hunter_positions = [hunter_position]
        hunter_headings = [hunter_heading]
        OTHER = (measurements, hunter_positions, hunter_headings) # now I can keep track of history
    else: # not the first time, update my history
        OTHER[0].append(target_measurement)
        OTHER[1].append(hunter_position)
        OTHER[2].append(hunter_heading)
        measurements, hunter_positions, hunter_headings = OTHER # now I can always refer to these variables
    
    heading_to_target = get_heading(hunter_position, target_measurement)
    heading_difference = heading_to_target - hunter_heading
    turning =  heading_difference # turn towards the target
    distance = max_distance # full speed ahead!
    return turning, distance, OTHER

'''
target = robot(0.0, 10.0, 0.0, 2*pi / 30, 1.5)
measurement_noise = .05*target.distance
#target.set_noise(0.0, 0.0, measurement_noise)

hunter = robot(-10.0, -10.0, 0.0)

print demo_grading(hunter, target, next_move)
'''




