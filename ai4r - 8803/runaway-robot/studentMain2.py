# ----------
# Part Two
#
# Now we'll make the scenario a bit more realistic. Now Traxbot's
# sensor measurements are a bit noisy (though its motions are still
# completetly noise-free and it still moves in an almost-circle).
# You'll have to write a function that takes as input the next
# noisy (x, y) sensor measurement and outputs the best guess 
# for the robot's next position.
#
# ----------
# YOUR JOB
#
# Complete the function estimate_next_pos. You will be considered 
# correct if your estimate is within 0.01 stepsizes of Traxbot's next
# true position. 
#
# ----------
# GRADING
# 
# We will make repeated calls to your estimate_next_pos function. After
# each call, we will compare your estimated position to the robot's true
# position. As soon as you are within 0.01 stepsizes of the true position,
# you will be marked correct and we will tell you how many steps it took
# before your function successfully located the target bot.

# These import steps give you access to libraries which you may (or may
# not) want to use.
from robot import *  # Check the robot.py tab to see how this works.
from math import *
from matrix import * # Check the matrix.py tab to see how this works.
import random
import time
import numpy
from scipy import optimize

def angle_trunc(a):
    return ((a + pi) % (pi * 2)) - pi

#adaptation of example 2 from https://www.programcreek.com/python/example/52479/scipy.optimize.leastsq
def _optLeastSqCircle(x,y):
    def calcR(x,y, xc, yc):
      '''
        Calculate distance of each point from the center (xc, yc) .
      '''
      return numpy.sqrt((x-xc)**2 + (y-yc)**2)

    def f(c, x, y):
      '''
        Calculate the algebraic distance between the data points and the mean 
        circle centered at c = (xc, yc).
      '''
      Ri = calcR(x, y, *c)
      return Ri - Ri.mean()   

    x_m = numpy.mean(x)
    y_m = numpy.mean(y)
    centre_estimate = x_m, y_m
    centre, ier = optimize.leastsq(f, centre_estimate, args=(x,y))
    xc, yc = centre
    Ri = calcR(x, y, *centre)
    R = Ri.mean()

    residuals = numpy.sqrt((Ri - R)**2)
    
    return xc, yc, R, residuals 


'''
    if(OTHER == None):
        #init OTHER as a list to collect measured points
        OTHER = [[],[]]
    
    OTHER[0].append(measurement[0])
    OTHER[1].append(measurement[1])

    if(len(OTHER[0])>2):
        x,y,r,rs = _optLeastSqCircle(OTHER[0],OTHER[1])


        #print "least squares result : "+str(x) +" ",y
        #get the theta between this measurement and last measurement
        x0 = OTHER[0][-2]
        y0 = OTHER[1][-2]

        deltax = (x0)-(x)
        deltay = (y0)-(y)

        #polar math
        r0 = sqrt(deltax**2+deltay**2)
        rt0 = atan2(deltay,deltax)
        rt1 = atan2((measurement[1]-y),(measurement[0]-x))
        rt2 = rt1 +(rt1-rt0)

        x2 = r0*cos(rt2)+x
        y2 = r0*sin(rt2)+y

        xy_estimate = (x2,y2)
    else:
        #return measurement... for now
        xy_estimate = measurement
'''





initial_xy = [0., 0.]
#initial matrixs


F = matrix([[1.,0,   1,0],
            [0,1.,   0,1],
            [0,0,   1.,0],
            [0,0,   0,1.]]) # next state function: generalize the 2d version to 4d
H = matrix([[1.,0.,0.,0.],
            [0.,1.,0.,0.]]) # measurement function: reflect the fact that we observe x and y but not the two velocities
R = matrix([[0.05,0],[0,0.05]]) # measurement uncertainty: use 2x2 matrix with 0.1 as main diagonal
I = matrix([[1.,0,0,0],
            [0,1.,0,0],
            [0,0,1.,0],
            [0,0,0,1.]]) # 4d identity matrix
x = matrix([[initial_xy[0]], [initial_xy[1]], [0.], [0.]])
u = matrix([[0.], [0.], [0.], [0.]])
Q =  matrix([[.1,0,   0,0],
            [0,.1,   0,0],
            [0,0,.1,0],
            [0,0,   0,.1]])

#starting from ps2
def kalhman_filter(x, P, measurement):

    #print measurement
    z = [measurement[0],measurement[1]]
    #print z
    # prediction
    x = (F * x) + u
    P = (F * P * F.transpose()) + Q

    
    # measurement update
    Z = matrix([z])

    y = Z.transpose() - (H * x)
    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    x = x + (K * y)
    P = (I - (K * H)) * P 

    return x,P

def method_1_straight_kal(measurement, OTHER = None):

    if(OTHER == None):
        P = matrix([[1.,0,   0,0],
            [0,1.,   0,0],
            [0,0,1.,0],
            [0,0,   0,1.]])
        x = matrix([[initial_xy[0]], [initial_xy[1]], [0.], [0.]])
        OTHER = []
    else:
        x = OTHER[0]
        P = OTHER[1]

    #convert measurement to polar
    r0 = sqrt(measurement[0]**2+measurement[1]**2)
    rt0 = atan2(measurement[0],measurement[1])

    x,P = kalhman_filter(x,P,(r0,rt0))
    OTHER = (x,P)

    print "x: ",x

    r0 = x[0][0]
    rt0 = x[1][0]

    x1 = r0*cos(rt0)
    y1 = r0*sin(rt0)

    xy_estimate = (y1,x1)
    # You must return y_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.

    return xy_estimate, OTHER

def method_2_assisted_kal(measurement, OTHER):

    if(OTHER == None):
        P = matrix([[1.,0,   0,0],
            [0,1.,   0,0],
            [0,0,1.,0],
            [0,0,   0,1.]])
        x = matrix([[initial_xy[0]], [initial_xy[1]], [0.], [0.]])
        OTHER = []
        l = [[],[]]
    else:
        x = OTHER[0]
        P = OTHER[1]
        l = OTHER[2]

    l[0].append(measurement[0])
    l[1].append(measurement[1])

    if(len(l[0]) > 5):
        centerx,centery,r,rs = _optLeastSqCircle(l[0],l[1])


        #print "circle center; "+str(centerx)+" ",centery

        deltax = measurement[0]-centerx
        deltay = measurement[1]-centery

        r0 = sqrt(deltax**2+deltay**2)
        rt0 = atan2(deltay,deltax)

        if(rt0<0):
            flipY = True
            rt0 = atan2(-deltay,deltax)
        else:
            flipY = False

    else:
        r0 = sqrt(measurement[0]**2+measurement[1]**2)
        rt0 = atan2(measurement[1],measurement[0])

    #rt0 = angle_trunc(rt0)
    x,P = kalhman_filter(x,P,(r0,rt0))
    OTHER = (x,P,l)

    r0 = x[0][0]
    rt0 = x[1][0]

    r1  = r0 + x[2][0]
    rt1 = rt0 + x[3][0]

    '''
    print "measurement : ",measurement
    print "rs : "+str(r0)+" ",rt0
    print "x : ",x
    '''
    if(len(l[0]) > 5):
        x1 = (r0*cos(rt0))+centerx
        y1 = (r0*sin(rt0))+centery

        x2 = (r1*cos(rt1))+centerx

        if(flipY):
            y2 = -(r1*sin(rt1))+centery
        else:
            y2 = (r1*sin(rt1))+centery
    else:
        x2 = r0*cos(rt0)
        y2 = r0*sin(rt0)

    xy_estimate = (x2,y2)

    # You must return xy_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.

    return xy_estimate, OTHER

def method_3_geometry(measurement, OTHER = None):
    if(OTHER == None):
        #init OTHER as a list to collect measured points
        OTHER = [[],[]]
    
    OTHER[0].append(measurement[0])
    OTHER[1].append(measurement[1])

    if(len(OTHER[0])>2):
        x,y,r,rs = _optLeastSqCircle(OTHER[0],OTHER[1])

        #print "least squares result : "+str(x) +" ",y
        #get the theta between this measurement and last measurement
        x0 = OTHER[0][-2]
        y0 = OTHER[1][-2]

        deltax = (x0)-(x)
        deltay = (y0)-(y)

        #polar math
        r0 = sqrt(deltax**2+deltay**2)
        rt0 = atan2(deltay,deltax)
        rt1 = atan2((measurement[1]-y),(measurement[0]-x))
        rt2 = rt1 +(rt1-rt0)

        x2 = r0*cos(rt2)+x
        y2 = r0*sin(rt2)+y

        xy_estimate = (x2,y2)
    else:
        #return measurement... for now
        xy_estimate = measurement

    # You must return xy_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.
    return xy_estimate, OTHER 

# This is the function you have to write. Note that measurement is a 
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be 
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""
    #xy_estimate, OTHER = method_1_straight_kal(measurement, OTHER)
    xy_estimate, OTHER = method_2_assisted_kal(measurement, OTHER)
    #xy_estimate, OTHER = method_3_geometry(measurement, OTHER)

    return xy_estimate, OTHER

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# This is here to give you a sense for how we will be running and grading
# your code. Note that the OTHER variable allows you to store any 
# information that you want. 

'''
def demo_grading(estimate_next_pos_fcn, target_bot, OTHER = None):
    localized = False
    distance_tolerance = 0.01 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    while not localized and ctr <= 1000:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        if error <= distance_tolerance:
            print "You got it right! It took you ", ctr, " steps to localize."
            localized = True
        if ctr == 1000:
            print "Sorry, it took you too many steps to localize the target."
    return localized
'''

'''
def demo_grading(estimate_next_pos_fcn, target_bot, OTHER = None):
    localized = False
    distance_tolerance = 0.01 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    #For Visualization
    import turtle    #You need to run this locally to use the turtle module
    window = turtle.Screen()
    window.bgcolor('white')
    size_multiplier= 25.0  #change Size of animation
    origin = turtle.Turtle()
    origin.shape('circle')
    origin.color('red')
    origin.resizemode('user')
    origin.shapesize(0.2, 0.2, 0.2)
    broken_robot = turtle.Turtle()
    broken_robot.shape('turtle')
    broken_robot.color('green')
    broken_robot.resizemode('user')
    broken_robot.shapesize(0.1, 0.1, 0.1)
    measured_broken_robot = turtle.Turtle()
    measured_broken_robot.shape('circle')
    measured_broken_robot.color('red')
    measured_broken_robot.resizemode('user')
    measured_broken_robot.shapesize(0.1, 0.1, 0.1)
    prediction = turtle.Turtle()
    prediction.shape('arrow')
    prediction.color('blue')
    prediction.resizemode('user')
    prediction.shapesize(0.1, 0.1, 0.1)
    prediction.penup()
    broken_robot.penup()
    measured_broken_robot.penup()
    #End of Visualization
    while not localized and ctr <= 1000:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        print "error = ",error
        #if error <= distance_tolerance:
        #    print "You got it right! It took you ", ctr, " steps to localize."
        #    localized = True
        if ctr == 10:
            print "Sorry, it took you too many steps to localize the target."
        #More Visualization
        measured_broken_robot.setheading(target_bot.heading*180/pi)
        measured_broken_robot.goto(measurement[0]*size_multiplier, measurement[1]*size_multiplier-200)
        measured_broken_robot.stamp()
        broken_robot.setheading(target_bot.heading*180/pi)
        broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-200)
        broken_robot.stamp()
        prediction.setheading(target_bot.heading*180/pi)
        prediction.goto(position_guess[0]*size_multiplier, position_guess[1]*size_multiplier-200)
        prediction.stamp()
        origin.goto(0,0)
        origin.stamp()
        time.sleep(0.05)

        #End of Visualization
    return localized
'''

# This is a demo for what a strategy could look like. This one isn't very good.
def naive_next_pos(measurement, OTHER = None):
    """This strategy records the first reported position of the target and
    assumes that eventually the target bot will eventually return to that 
    position, so it always guesses that the first position will be the next."""
    if not OTHER: # this is the first measurement
        OTHER = measurement
    xy_estimate = OTHER 
    return xy_estimate, OTHER

'''
# This is how we create a target bot. Check the robot.py file to understand
# How the robot class behaves.
test_target = robot(2.1, 4.3, 0.5, 2*pi / 34.0, 1.5)
measurement_noise = 0.05 * test_target.distance
test_target.set_noise(0.0, 0.0, measurement_noise)

demo_grading(estimate_next_pos, test_target)
'''



