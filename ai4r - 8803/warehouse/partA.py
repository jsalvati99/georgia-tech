'''
=== Introduction ===

In this problem, you will build a planner that helps a robot
  find the best path through a warehouse filled with boxes
  that it has to pick up and deliver to a dropzone.

Your file must be called `partA.py` and must have a class
  called `DeliveryPlanner`.
This class must have an `__init__` function that takes three 
  arguments: `self`, `warehouse`, and `todo`.
The class must also have a function called `plan_delivery` that 
  takes a single argument, `self`.

=== Input Specifications ===

`warehouse` will be a list of m strings, each with n characters,
  corresponding to the layout of the warehouse. The warehouse is an
  m x n grid. warehouse[i][j] corresponds to the spot in the ith row
  and jth column of the warehouse, where the 0th row is the northern
  end of the warehouse and the 0th column is the western end.

The characters in each string will be one of the following:

'.' (period) : traversable space. The robot may enter from any adjacent space.
'#' (hash) : a wall. The robot cannot enter this space.
'@' (dropzone): the starting point for the robot and the space where all boxes must be delivered.
  The dropzone may be traversed like a '.' space.
[0-9a-zA-Z] (any alphanumeric character) : a box. At most one of each alphanumeric character 
  will be present in the warehouse (meaning there will be at most 62 boxes). A box may not
  be traversed, but if the robot is adjacent to the box, the robot can pick up the box.
  Once the box has been removed, the space functions as a '.' space.

For example, 
  warehouse = ['1#2',
               '.#.',
               '..@']
  is a 3x3 warehouse.
  - The dropzone is at the warehouse cell in row 2, column 2.
  - Box '1' is located in the warehouse cell in row 0, column 0.
  - Box '2' is located in the warehouse cell in row 0, column 2.
  - There are walls in the warehouse cells in row 0, column 1 and row 1, column 1.
  - The remaining five warehouse cells contain empty space. (The dropzone is empty space)
#
The argument `todo` is a list of alphanumeric characters giving the order in which the 
  boxes must be delivered to the dropzone. For example, if 
  todo = ['1','2']
  is given with the above example `warehouse`, then the robot must first deliver box '1'
  to the dropzone, and then the robot must deliver box '2' to the dropzone.

=== Rules for Movement ===

- Two spaces are considered adjacent if they share an edge or a corner.
- The robot may move horizontally or vertically at a cost of 2 per move.
- The robot may move diagonally at a cost of 3 per move.
- The robot may not move outside the warehouse.
- The warehouse does not "wrap" around.
- As described earlier, the robot may pick up a box that is in an adjacent square.
- The cost to pick up a box is 4, regardless of the direction the box is relative to the robot.
- While holding a box, the robot may not pick up another box.
- The robot may put a box down on an adjacent empty space ('.') or the dropzone ('@') at a cost
  of 2 (regardless of the direction in which the robot puts down the box).
- If a box is placed on the '@' space, it is considered delivered and is removed from the ware-
  house.
- The warehouse will be arranged so that it is always possible for the robot to move to the 
  next box on the todo list without having to rearrange any other boxes.

An illegal move will incur a cost of 100, and the robot will not move (the standard costs for a 
  move will not be additionally incurred). Illegal moves include:
- attempting to move to a nonadjacent, nonexistent, or occupied space
- attempting to pick up a nonadjacent or nonexistent box
- attempting to pick up a box while holding one already
- attempting to put down a box on a nonadjacent, nonexistent, or occupied space
- attempting to put down a box while not holding one

=== Output Specifications ===

`plan_delivery` should return a LIST of moves that minimizes the total cost of completing
  the task successfully.
Each move should be a string formatted as follows:

'move {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot moves
  to and '{j}' is replaced by the column-coordinate of the space the robot moves to

'lift {x}', where '{x}' is replaced by the alphanumeric character of the box being picked up

'down {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot puts 
  the box, and '{j}' is replaced by the column-coordinate of the space the robot puts the box

For example, for the values of `warehouse` and `todo` given previously (reproduced below):
  warehouse = ['1#2',
               '.#.',
               '..@']
  todo = ['1','2']
`plan_delivery` might return the following:
  ['move 2 1',
   'move 1 0',
   'lift 1',
   'move 2 1',
   'down 2 2',
   'move 1 2',
   'lift 2',
   'down 2 2']

=== Grading ===

- Your planner will be graded against a set of test cases, each equally weighted.
- If your planner returns a list of moves of total cost that is K times the minimum cost of 
  successfully completing the task, you will receive 1/K of the credit for that test case.
- Otherwise, you will receive no credit for that test case. This could happen for one of several 
  reasons including (but not necessarily limited to):
  - plan_delivery's moves do not deliver the boxes in the correct order.
  - plan_delivery's output is not a list of strings in the prescribed format.
  - plan_delivery does not return an output within the prescribed time limit.
  - Your code raises an exception.

=== Additional Info ===


- You may add additional classes and function
 needed provided they are all in the file `partA.py`.
- Upload partA.py to  Canvas in the Assignments section. Do not put it into an 
  archive with other files.
- Your partA.py file must not execute any code when imported.
- Ask any questions about the directions or specifications on Piazza.
'''
from copy import deepcopy

class DeliveryPlanner:

    def __init__(self, warehouse, todo):
        self.w = warehouse
        self.t = todo
        pass

    actions = [[ -1,0],
               [ 0,-1], # go left
               [ 1, 0], # go down
               [ 0, 1],
               [-1,-1],
               [-1,1],
               [1,-1],
               [1,1]] # go right

    def getCost(self,action):
      cost = 2
      for i in range(len(self.actions)):
        if self.actions[i] == action:
          if i <4:
            cost = 2
            break
          else:
            cost = 3
            break
      return cost


    def is_cell_free(self,wareH,cell, option=None):
      result = False
      if(wareH[cell[0]][cell[1]] != '#'):
        result = True

      return result

    def is_adjacent_to(self,cell1,cell2):
      
      result = False

      for action in self.actions:

        newCell = [cell1[0]+action[0],cell1[1]+action[1]]

        #print "is adjacent to: "+str(newCell)+ "   :  ",str(cell2)

        if (newCell[0] == cell2[0] and newCell[1] == cell2[1]):
          result = True
          break

      return result

    
    def aStarHeuristic(self,wareH, box):
      #there should be a way to optimize given the list of boxes...
      #for now just generate it each time using the first box
      #this still treats diagonals the same. not sure if that will matter later

      heuristic = [[9999 for x in range(len(self.w[0]))] for y in range(len(self.w))] 
      
      for row in range(len(wareH)):
        for col in range(len(wareH[0])):
          heuristic[row][col] = 9999

      openList = [[box[0],box[1]]]
      visited = []

      done = False
      cost = 1

      height = len(wareH)
      width = len(wareH[0])

      while not done:
        if len(openList) == 0:
            done = True
        else:
          nextItem = openList.pop()
          visited.append(nextItem)

          for action in self.actions:

            nextX = nextItem[0]+action[0]
            nextY = nextItem[1]+action[1]
            adjacent = [nextX,nextY]

            if(nextX < height and nextX >= 0 and nextY < width and nextY >= 0):
              if self.is_cell_free(wareH,adjacent,option=box[2]) and adjacent not in visited: 
                openList.append(adjacent)
                if(cost < heuristic[nextX][nextY]):
                  heuristic[nextX][nextY] = cost
        cost += 1

      print "heuristic : ",heuristic

      return heuristic

    def astar_until_box(self,robotPos, wareH,nextBox):
      #first box is always reachable, generate heuristic starting at the first box
      #warehouse is not cyclic, leverage that to not expand unnessarily
      path = []
      openList = [[0,robotPos[0],robotPos[1],0]]
      visited = []

      height = len(wareH)
      width = len(wareH[0])

      reachedGoal = False
      noSolution = False

      #heuristic = self.aStarHeuristic(wareH, nextBox)

      for row in range(len(self.w)):
        for col in range(len(self.w[0])):
          localCostMap = [[9999 for x in range(len(self.w[0]))] for y in range(len(self.w[0]))] 

      goal = nextBox

      localCostMap[robotPos[0]][robotPos[1]] = 0

      #adaptation of the "12. quiz: implement A*" in the search lecture
      while not reachedGoal and not noSolution:
        if len(openList) == 0:
          noSolution = True
          #print "didnt find a solution"
        else:
          openList.sort()
          openList.reverse()
          nextItem = openList.pop()
          f = nextItem[0]
          x = nextItem[1]
          y = nextItem[2]
          g = nextItem[3]
          
          if x == goal[0] and y == goal[1]:
            #print "reached goal : ",goal
            reachedGoal = True
            localCostMap[x][y] = f
          else:
            for action in self.actions:
              x2 = x + action[0]
              y2 = y + action[1]
              if x2 >= 0 and x2 < len(wareH) and y2 >=0 and y2 < len(wareH[0]):
                if self.is_cell_free(wareH,[x2,y2],option=goal[2]):
                  if [x2,y2] not in visited:
                    g2 = g + self.getCost(action)
                    f2 = g2+(abs((x2-goal[0])+(y2-goal[1])))

                    if(self.w[x2][y2] != '.' and self.w[x2][y2] != '@'):
                      f2 += 100 #high cost for boxes

                    if f2 < localCostMap[x2][y2]:
                      localCostMap[x2][y2] = f2

                    openList.append([f2, x2, y2,g2])
                    visited.append([x2,y2])     

      #print "local cost map : "
      #for i in range(len(localCostMap)):
      #  print localCostMap[i]
      #print "warehouse : "
      #for i in range(len(self.w)):
      #  print self.w[i]

      #print "return the path"
      #return the path!  
      actionList = []
      currentCell = goal

      atBox = False

      while not atBox:

        #print "currentCell : ",currentCell 
        #print "nextBox : ",nextBox

        if(currentCell[0] == robotPos[0] and currentCell[1] == robotPos[1]):
          atBox = True
        else:
          minVal = 999
          for action in self.actions:

            nextCell = [currentCell[0]+action[0],currentCell[1]+action[1]]
            x2 = nextCell[0]
            y2 = nextCell[1]
            if x2 >= 0 and x2 < len(wareH) and y2 >=0 and y2 < len(wareH[0]):
              if(localCostMap[nextCell[0]][nextCell[1]] < minVal):
                bestAction = nextCell
                minVal = localCostMap[nextCell[0]][nextCell[1]]

          actionList.append(bestAction)
          currentCell = bestAction

      #print "action list is : ",actionList 

      return actionList


    def plan_delivery(self):

        boxes = []

        #process warehouse and determine where things are
        foundGoal = False  #if we dont see where the drop off is, assume its where the robot is
        for row in range(len(self.w)):
          for col in range(len(self.w[0])):
            if self.w[row][col] == '@':
              goalPos = (row,col)
              robotStart = (row,col)
              foundGoal = True

            for item in self.t:
              if self.w[row][col] == item:
                boxes.append([row,col,item])

        #print "the boxes are : ",boxes

        if foundGoal == False:
          goalPos = robotStart

        #create a local costmap version
        '''
        for row in range(len(self.w)):
          for col in range(len(self.w[0])):
            localCostMap = [[0 for x in range(row)] for y in range(col)] 
        '''

        #while there are boxes
        #  get path to closest box
        #    closest box can be breath first search
        #  determine moves to get to the box and add it to the current moves list
        #  remove the box from the todo list
        #return!
        robotPos = robotStart
        moves = []
        nextBox = 0
        firstRun = True

        #print "todo list : ",self.t

        while len(self.t) > 0:

          boxtoGet = self.t.pop(0)
          for b in boxes:
            #print "b is ... : ",b
            #print "box to get : ",boxtoGet
            if b[2] == boxtoGet:
              nextBox = b

          #print "nextBox : ",nextBox

          actionList = self.astar_until_box(robotPos,self.w,nextBox)
          boxLocation = nextBox
          box = self.w[boxLocation[0]][boxLocation[1]]
          #last item in the actionList is the robotsPosition
          #if it started at the start, take it out else leave it
          #actionList.pop(len(actionList)-1)
          
          if(robotPos == robotStart):
            actionList = actionList[:-1]

          actionList.reverse()
          #print " make move list from action list : ",actionList 
          #print "robot position : ",robotPos

          for action in actionList:

            if(action != robotPos):
              moves.append('move {0} {1}'.format(action[0],action[1]))
              robotPos = action

          moves.append('lift {}'.format(box))


          #if(len(actionList) > 0):
          #  startLoc = actionList.pop()

          for action in actionList[::-1]:
            if(action != robotPos):
                moves.append('move {0} {1}'.format(action[0],action[1]))
                robotPos = action

            if(self.is_adjacent_to(robotPos,robotStart)):
              break

          if(robotPos == robotStart):
            #robot has box and is at start position, move...to where the box was
            moves.append('move {0} {1}'.format(boxLocation[0],boxLocation[1]))
            robotPos = [boxLocation[0],boxLocation[1]]

          moves.append('down {} {}'.format(robotStart[0],robotStart[1]))
        
        #print "final move list !: ",moves
        #print moves
        return moves
