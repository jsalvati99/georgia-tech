"""
=== Introduction ===

In this problem, you will again build a planner that helps a robot
  find the best path through a warehouse filled with boxes
  that it has to pick up and deliver to a dropzone. Unlike Part A,
  however, in this problem the robot is moving in a continuous world
  (albeit in discrete time steps) and has constraints on the amount
  it can turn its wheels in a given time step.

Your file must be called `partB.py` and must have a class
  called `DeliveryPlanner`.
This class must have an `__init__` function that takes five 
  arguments: `self`, `warehouse`, `todo`, `max_distance`, and
  `max_steering`.
The class must also have a function called `plan_delivery` that 
  takes a single argument, `self`.

=== Input Specifications ===

`warehouse` will be a list of m strings, each with n characters,
  corresponding to the layout of the warehouse. The warehouse is an
  m x n grid. warehouse[i][j] corresponds to the spot in the ith row
  and jth column of the warehouse, where the 0th row is the northern
  end of the warehouse and the 0th column is the western end.

The characters in each string will be one of the following:

'.' (period) : traversable space.
'#' (hash) : a wall. If the robot contacts a wall space, it will crash.
'@' (dropzone): the space where all boxes must be delivered. The dropzone may be traversed like 
  a '.' space.

Each space is a 1 x 1 block. The upper-left corner of space warehouse[i][j] is at the point (j,-i) in
  the plane. Spaces outside the warehouse are considered walls; if any part of the robot leaves the 
  warehouse, it will be considered to have crashed into the exterior wall of the warehouse.

For example, 
  warehouse = ['.#.',
               '.#.',
               '..@']
  is a 3x3 warehouse. The dropzone is at space (2,-2) and there are walls at spaces (1,0) 
  and (1,-1). The rest of the warehouse is empty space.

The robot is a circle of radius 0.25. The robot begins centered in the dropzone space.
  The robot's initial bearing is 0.

The argument `todo` is a list of points representing the center point of each box.
  todo[0] is the first box which must be delivered, followed by todo[1], and so on.
  Each box is a square of size 0.2 x 0.2. If the robot contacts a box, it will crash.

The arguments `max_distance` and `max_steering` are parameters constraining the movement
  of the robot on a given time step. They are described more below.

=== Rules for Movement ===

- The robot may move any distance between 0 and `max_distance` per time step.
- The robot may set its steering angle anywhere between -`max_steering` and 
  `max_steering` per time step. A steering angle of 0 means that the robot will
  move according to its current bearing. A positive angle means the robot will 
  turn counterclockwise by `steering_angle` radians; a negative steering_angle 
  means the robot will turn clockwise by abs(steering_angle) radians.
- Upon a movement, the robot will change its steering angle instantaneously to the 
  amount indicated by the move, and then it will move a distance in a straight line in its
  new bearing according to the amount indicated move.
- The cost per move is 1 plus the amount of distance traversed by the robot on that move.

- The robot may pick up a box whose center point is within 0.5 units of the robot's center point.
- If the robot picks up a box, it incurs a total cost of 2 for that move (this already includes 
  the 1-per-move cost incurred by the robot).
- While holding a box, the robot may not pick up another box.
- The robot may put a box down at a total cost of 1.5 for that move. The box must be placed so that:
  - The box is not contacting any walls, the exterior of the warehouse, any other boxes, or the robot
  - The box's center point is within 0.5 units of the robot's center point
- A box is always oriented so that two of its edges are horizontal and the other two are vertical.
- If a box is placed entirely within the '@' space, it is considered delivered and is removed from the 
  warehouse.
- The warehouse will be arranged so that it is always possible for the robot to move to the 
  next box on the todo list without having to rearrange any other boxes.

- If the robot crashes, it will stop moving and incur a cost of 100*distance, where distance
  is the length it attempted to move that move. (The regular movement cost will not apply.)
- If an illegal move is attempted, the robot will not move, but the standard cost will be incurred.
  Illegal moves include (but are not necessarily limited to):
    - picking up a box that doesn't exist or is too far away
    - picking up a box while already holding one
    - putting down a box too far away or so that it's touching a wall, the warehouse exterior, 
      another box, or the robot
    - putting down a box while not holding a box

=== Output Specifications ===

`plan_delivery` should return a LIST of strings, each in one of the following formats.

'move {steering} {distance}', where '{steering}' is a floating-point number between
  -`max_steering` and `max_steering` (inclusive) and '{distance}' is a floating-point
  number between 0 and `max_distance`

'lift {b}', where '{b}' is replaced by the index in the list `todo` of the box being picked up
  (so if you intend to lift box 0, you would return the string 'lift 0')

'down {x} {y}', where '{x}' is replaced by the x-coordinate of the center point of where the box
  will be placed and where '{y}' is replaced by the y-coordinate of that center point
  (for example, 'down 1.5 -2.9' means to place the box held by the robot so that its center point
  is (1.5,-2.9)).

=== Grading ===

- Your planner will be graded against a set of test cases, each equally weighted.
- Each task will have a "baseline" cost. If your set of moves results in the task being completed
  with a total cost of K times the baseline cost, you will receive 1/K of the credit for the
  test case. (Note that if K < 1, this means you earn extra credit!)
- Otherwise, you will receive no credit for that test case. This could happen for one of several 
  reasons including (but not necessarily limited to):
  - plan_delivery's moves do not deliver the boxes in the correct order.
  - plan_delivery's output is not a list of strings in the prescribed format.
  - plan_delivery does not return an output within the prescribed time limit.
  - Your code raises an exception.

=== Additional Info ===

- You may add additional classes and functions as needed provided they are all in the file `partB.py`.
- Your partB.py file must not execute any code when it is imported. 
- Upload partB.py to Canvas in the Assignments section. Do not put it into an 
  archive with other files.
- Ask any questions about the directions or specifications on Piazza.
"""
import math

class DeliveryPlanner:

    def __init__(self, warehouse, todo, max_distance, max_steering):
          self.w = warehouse
          self.t = todo
          self.max_d = max_distance
          self.max_s = max_steering
          pass

    '''
    actions = [[ -1,0],
               [ 0,-1], # go left
               [ 1, 0], # go down
               [ 0, 1],
               [-1,-1],
               [-1,1],
               [1,-1],
               [1,1]] # go right
    '''
    actions = [[ -1,0],
               [ 0,-1], # go left
               [ 1, 0], # go down
               [ 0, 1]]

    def getCost(self,action):
      cost = 2
      for i in range(len(self.actions)):
        if self.actions[i] == action:
          if i <4:
            cost = 2
            break
          else:
            cost = 3
            break
      return cost


    def is_cell_free(self,wareH,cell, option=None):
      result = False
      if(wareH[cell[0]][cell[1]] != '#'):
        result = True

      return result

    def is_adjacent_to(self,cell1,cell2):
      
      result = False

      for action in self.actions:

        newCell = [cell1[0]+action[0],cell1[1]+action[1]]

        #print "is adjacent to: "+str(newCell)+ "   :  ",str(cell2)

        if (newCell[0] == cell2[0] and newCell[1] == cell2[1]):
          result = True
          break

      return result

    
    def aStarHeuristic(self,wareH, box):
      #there should be a way to optimize given the list of boxes...
      #for now just generate it each time using the first box
      #this still treats diagonals the same. not sure if that will matter later

      heuristic = [[9999 for x in range(len(self.w[0]))] for y in range(len(self.w))] 
      
      #print " box is : ",box

      if(abs(box[1]%1)>0.0):
        box1 = box[1]-0.5
      if(abs(box[0]%1)>0.0):
        box0 = box[0]-0.5

      box = [abs(box1),box0]

      for row in range(len(wareH)):
        for col in range(len(wareH[0])):
          heuristic[row][col] = 9999

      openList = [[box[1],box[0]]]
      visited = []

      done = False
      cost = 1

      height = len(wareH)
      width = len(wareH[0])

      #print "height: ",height
      #print "width: ",width

      while not done:
        if len(openList) == 0:
            done = True
        else:
          nextItem = openList.pop()
          visited.append(nextItem)

          for action in self.actions:

            nextX = nextItem[0]+action[0]
            nextY = nextItem[1]+action[1]
            adjacent = [nextX,nextY]

            if(nextX < height and nextX >= 0 and nextY < width and nextY >= 0):
              if self.is_cell_free(wareH,adjacent) and adjacent not in visited: 
                openList.append(adjacent)
                if(cost < heuristic[nextX][nextY]):
                  heuristic[nextX][nextY] = cost
        cost += 1

      #print "heuristic : ",heuristic

      return heuristic

    def astar_until_box(self,robotPos, wareH,nextBox,other=None):
      #first box is always reachable, generate heuristic starting at the first box
      #warehouse is not cyclic, leverage that to not expand unnessarily
      path = []
      openList = [[0,robotPos[0],robotPos[1],0]]
      visited = []

      height = len(wareH)
      width = len(wareH[0])

      reachedGoal = False
      noSolution = False

      #heuristic = self.aStarHeuristic(wareH, nextBox)

      for row in range(len(self.w)):
        for col in range(len(self.w[0])):
          localCostMap = [[9999 for x in range(len(self.w[0]))] for y in range(len(self.w[0]))] 


      #print " next box is : ",nextBox
      #print "nextbox : ",nextBox[0]
      #print "nextbox : ",nextBox[1]

      if(abs(nextBox[1]%1)>0.0):
        nextBox1 = nextBox[1]+0.5
      else:
        nextBox1 = nextBox[1]
      if(abs(nextBox[0]%1)>0.0):
        nextBox0 = nextBox[0]-0.5
      else:
        nextBox0 = nextBox[0]

      otherBoxes = []
      if other != None:
        for b in other:

          if(abs(b[1]%1)>0.0):
            b1 = b[1]+0.5
          else:
            b1 = b[1]
          if(abs(nextBox[0]%1)>0.0):
            b0 = b[0]-0.5
          else:
            b0 = b[0]

          otherBoxes.append([int(b1),int(b0)])


      goal = [abs(int(nextBox1)),int(nextBox0)]

   
      localCostMap[robotPos[0]][robotPos[1]] = 0

      #adaptation of the "12. quiz: implement A*" in the search lecture
      while not reachedGoal and not noSolution:
        if len(openList) == 0:
          noSolution = True
          #print "didnt find a solution"
        else:
          openList.sort()
          openList.reverse()
          nextItem = openList.pop()
          f = nextItem[0]
          x = nextItem[1]
          y = nextItem[2]
          g = nextItem[3]
          
          if x == goal[0] and y == goal[1]:
            #print "reached goal : ",goal
            reachedGoal = True
            localCostMap[x][y] = f
          else:
            for action in self.actions:
              x2 = x + action[0]
              y2 = y + action[1]
              if x2 >= 0 and x2 < len(wareH) and y2 >=0 and y2 < len(wareH[0]):
                if self.is_cell_free(wareH,[x2,y2]):
                  if [x2,y2] not in visited:
                    g2 = g + self.getCost(action)
                    f2 = g2+(abs((x2-goal[0])+(y2-goal[1])))

                    if(len(otherBoxes) > 0):
                      for b in otherBoxes:
                        if([x2,y2] == b):
                          f2 += 100

                    #if(self.w[x2][y2] != '.' and self.w[x2][y2] != '@'):
                    #  f2 += 100 #high cost for boxes

                    if f2 < localCostMap[x2][y2]:
                      localCostMap[x2][y2] = f2

                    openList.append([f2, x2, y2,g2])
                    visited.append([x2,y2])     

      #print "local cost map : "
      #for i in range(len(localCostMap)):
      #  print localCostMap[i]
      #print "warehouse : "
      #for i in range(len(self.w)):
      #  print self.w[i]

      #print "return the path"
      #return the path!  
      actionList = []
      currentCell = goal

      atBox = False

      while not atBox:

        #print "currentCell : ",currentCell 
        #print "nextBox : ",nextBox

        if(currentCell[0] == robotPos[0] and currentCell[1] == robotPos[1]):
          #print "made it to the box"
          atBox = True
        else:
          minVal = 999
          for action in self.actions:

            nextCell = [currentCell[0]+action[0],currentCell[1]+action[1]]
            x2 = nextCell[0]
            y2 = nextCell[1]

            if x2 >= 0 and x2 < len(wareH) and y2 >=0 and y2 < len(wareH[0]):
              if(localCostMap[nextCell[0]][nextCell[1]] < minVal):
                bestAction = nextCell
                minVal = localCostMap[nextCell[0]][nextCell[1]]

          actionList.append(bestAction)
          currentCell = bestAction

      #print "action list is : ",actionList 

      return actionList,goal

    # helper function to map all angles onto [-pi, pi]
    def angle_trunc(self,a):
      return ((a + math.pi) % (math.pi * 2)) - math.pi

    def compute_move(self,currentCell,targetCell, heading, distance=1.0):

      moves = []

      #atan to get the heading
      targetHeading = math.atan2((currentCell[0]-targetCell[0]),targetCell[1]-currentCell[1])
      finalHeading = heading

      
      #print "current cell: ",currentCell
      #print "next cell : ",targetCell
      #print "target heading: ",targetHeading
      #print "current heading: ",heading
      #print "difference : ",(abs(targetHeading - heading))
      
      difference = (abs(targetHeading-heading))

      if(difference == math.pi):
        #print "turn about!!"
        moves.append('move {0} {1}'.format((math.pi/2),0))
        moves.append('move {0} {1}'.format((math.pi/2),distance))
        finalHeading += math.pi
      else:

        if(difference > math.pi):

          if(targetHeading == math.pi and heading < 0.0):
            h = -math.pi/2
          elif (heading == math.pi and targetHeading < 0.0):
            h = math.pi/2
          moves.append('move {0} {1}'.format((h),distance))

        elif(difference > 0):
          moves.append('move {0} {1}'.format((targetHeading - heading),distance))
        elif(difference == 0):
          moves.append('move {0} {1}'.format(0,distance))


        finalHeading = targetHeading

      #finalHeading = self.angle_trunc(finalHeading)
      if(finalHeading > math.pi):
        finalHeading = finalHeading - (2*math.pi)

      if(abs(finalHeading) == math.pi):
        finalHeading = math.pi


      return moves,finalHeading

    def jump_onto_box(self,currentPos,boxPos, heading,box, atDelivery=False, deliveryLocation=[0,0]):

      moves = []

      #atan to get the heading


      #print "  "
      #print " jumping on to the box!"
     
      boxPos0 = boxPos[0]
      boxPos1 = boxPos[1]
      diffy = boxPos1-currentPos[1]
      diffx = boxPos0-currentPos[0]
      distance = math.sqrt(diffy*diffy+diffx*diffx)
      targetHeading = math.atan2((boxPos1-currentPos[1]),boxPos0-currentPos[0])
      difference = (abs(targetHeading-heading))


      if(distance > 0.7 and difference > 0.0):

        if(boxPos0%1.0 == 0):
          #print " modying box pose"
          boxPos0 += 0.20
          
        if(boxPos1%1.0 == 0):
          boxPos1 -= 0.20

      targetHeading = math.atan2((boxPos1-currentPos[1]),boxPos0-currentPos[0])
      finalHeading = heading

      difference = (abs(targetHeading-heading))
      diffy = boxPos1-currentPos[1]
      diffx = boxPos0-currentPos[0]
      distance = math.sqrt(diffy*diffy+diffx*diffx) - 0.40    #account for robot and box size
      th = targetHeading - heading

      if(difference > math.pi):
          th -= 2*math.pi
      if(difference == math.pi):
        moves.append('move {0} {1}'.format(math.pi/2,0))
        moves.append('move {0} {1}'.format(math.pi/2,distance))
      else:
        moves.append('move {0} {1}'.format(th,distance))

      moves.append('lift {}'.format(box))
      moves.append('move {0} {1}'.format((math.pi/2),0))

      if(atDelivery):
        #print "handling at delivery point case"
        moves.append('move {0} {1}'.format((math.pi/2),distance-0.25))
        moves.append('down {0} {1}'.format(deliveryLocation[0],deliveryLocation[1]))
        moves.append('move {0} {1}'.format(0,0.25))


        if(difference == math.pi):
          moves.append('move {0} {1}'.format(-(th/2),0))
          moves.append('move {0} {1}'.format(-(th/2),0))
        else:
          moves.append('move {0} {1}'.format(-(th),0))

      else:
        moves.append('move {0} {1}'.format((math.pi/2),distance))

        if(difference == math.pi):
          moves.append('move {0} {1}'.format(-(th/2),0))
          moves.append('move {0} {1}'.format(-(th/2),0))
        else:
          moves.append('move {0} {1}'.format(-(th),0))

      #for m in moves:
      #  print m

      finalHeading += math.pi

      #finalHeading = self.angle_trunc(finalHeading)
      if(finalHeading > math.pi):
        finalHeading = finalHeading - (2*math.pi)

      if(abs(finalHeading) == math.pi):
        finalHeading = math.pi

      return moves,finalHeading

    def plan_delivery(self):

        boxes = []

        #process warehouse and determine where things are
        foundGoal = False  #if we dont see where the drop off is, assume its where the robot is
        for row in range(len(self.w)):
          for col in range(len(self.w[0])):
            if self.w[row][col] == '@':
              goalPos = (row,col)
              robotStart = (row,col)
              foundGoal = True

        #print "the boxes are : ",boxes

        if foundGoal == False:
          goalPos = robotStart

        #create a local costmap version
        '''
        for row in range(len(self.w)):
          for col in range(len(self.w[0])):
            localCostMap = [[0 for x in range(row)] for y in range(col)] 
        '''

        #while there are boxes
        #  get path to closest box
        #    closest box can be breath first search
        #  determine moves to get to the box and add it to the current moves list
        #  remove the box from the todo list
        #return!
        robotPos = robotStart
        moves = []
        nextBox = 0
        firstRun = True
        currentHeading = 0 
        box = 0

        #print "todo list : ",self.t

        while len(self.t) > 0:

          boxtoGet = self.t.pop(0)

          actionList,goal = self.astar_until_box(robotPos,self.w,boxtoGet,self.t)
          
          if(robotPos == robotStart):
            actionList = actionList[:-1]

          actionList.reverse()


          #print " make move list from action list : ",actionList 
          #print "robot position : ",robotPos

         
          for action in actionList:

            if(action != robotPos):

              #action is in [y,x]
              #so is robot pose [y,x]  
              # but move is heading, direction
              result,heading = self.compute_move(robotPos,action,currentHeading)
              currentHeading = heading

              for r in result:
                moves.append(r)

              robotPos = action

          if(len(actionList) > 0):
            #move to not quite the center of the goal to try and snag the box and then go back to the path
            #print ""
            #print "jump to the box"
            beforeBox = robotPos
            robotRealPos = [robotPos[1]+0.5,-(robotPos[0]+0.5)]

            result, heading = self.jump_onto_box(robotRealPos,boxtoGet,currentHeading,box)

            for r in result:
              moves.append(r)

            currentHeading = heading

            robotPos = beforeBox

            for action in actionList[::-1]:
              if(action != robotPos):

                result,heading = self.compute_move(robotPos,action,currentHeading)
                currentHeading = heading

                for r in result:
                  moves.append(r)

                robotPos = action

              if(self.is_adjacent_to(robotPos,robotStart)):
                break

          if(robotPos == robotStart):

            #box is next to the start, run a modified "jump to box" that handles the delivery 
            robotRealPos = [robotPos[1]+0.5,-(robotPos[0]+0.5)]
            result,heading = self.jump_onto_box(robotRealPos,boxtoGet,currentHeading,box,atDelivery=True,deliveryLocation=[goalPos[1]+0.5,-(goalPos[0]+0.5)])
            #robot has box and is at start position, move...to where the box was
            for r in result:
              moves.append(r)
            currentHeading = heading
          else:
            #move to not quite the center of the goal to try and snag the box and then go back to the path
            #print ""
            #print "jump to the goal"
            beforeBox = robotPos

            result,heading = self.compute_move(robotPos,goalPos,currentHeading,distance=0.52)
            currentHeading = heading

            for r in result:
              moves.append(r)

            robotPos = goalPos


            moves.append('down {} {}'.format(goalPos[1]+0.5,-(goalPos[0]+0.5)))

            #print ""
            #print "now jump back!"

            result,heading = self.compute_move(robotPos,beforeBox,currentHeading,distance=0.52)
            currentHeading = heading

            for r in result:
              moves.append(r)

            robotPos = beforeBox


          box += 1
        
        #print " "
        #print "final list :"
        #for m in moves:
        #  print m
       
        #print moves


        return moves
