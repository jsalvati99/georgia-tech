"""
 === Introduction ===
 
   A few months ago a new rover was sent to McMurdo Station in the Antarctic. The rover is a technical marvel
   as it is equipped with the latest scientific sensors and analyzers capable of surviving the harsh climate of the
   South Pole.  The goal is for the rover to reach a series of test sites and perform scientific sampling and analysis.
   Due to the extreme conditions, the rover will be air dropped via parachute into the test area.  The good news is
   the surface is smooth and free from any type of obstacles, the bad news is the surface is entirely ice which may 
   introduce noise into your rovers movements.  The station scientists are ready to deploy the new rover, but first 
   we need to create and test the planning software that will be used on board to ensure it can complete it's goals.

   The assignment is broken up into two parts.

   Part A:
        Create a SLAM implementation to process a series of landmark (beacon) measurements and movement updates.

        Hint: A planner with an unknown number of motions works well with an online version of SLAM.

    Part B:
        Here you will create the planner for the rover.  The rover does unfortunately has a series of limitations:

        - Start position
          - The rover will land somewhere within range of at least 3 or more landmarks (beacons or sites) for measurements.

        - Measurements
          - Measurements will come from beacons and test sites within range of the rover's antenna horizon.
            * The format is {'landmark id':{'distance':0.0, 'bearing':0.0, 'type':'beacon'}, ...}
          - Beacons and test sites will always return a measurement if in range.

        - Movements
          - Action: 'move 1.570963 1.0'
            * The rover will turn counterclockwise 90 degrees and then move 1.0
          - stochastic due to the icy surface.
          - if max distance or steering is exceeded, the rover will not move.

        - Samples
          - Provided as list of x and y coordinates, [[0., 0.], [1., -3.5], ...]
          - Action: 'sample'
            * The rover will attempt to take a sample at the current location.
          - A rover can only take a sample once per requested site.
          - The rover must be within 0.25 distance to successfully take a sample.
            * Hint: Be sure to account for floating point limitations
          - The is a 100ms penalty if the robot is requested to sample a site not on the list or if the site has
            previously been sampled.
          - You may temporarily use sys.stdout = open('stdout.txt', 'w') to directly print data if necessary, HOWEVER, you MUST REMOVE these lines before submitting your code or it will keep our testing framework from giving you a grade (larger than a zero). 

        The rover will always execute a measurement first, followed by an action.

        The rover will have a time limit of 5 seconds to find and sample all required sites.
"""
from math import *
from robot import *
from matrix import *
from copy import deepcopy

class SLAM:
    """Create a basic SLAM module.
    """
    def __init__(self, init_x=0.0, init_y=0.0, motion_noise=0.05,measurement_noise=1.15):
        """Initialize SLAM components here.
        """
        self.dim = 2 
        self.Omega = matrix()
        self.Omega.zero(self.dim,self.dim)
        self.Omega.value[0][0] = 1.0
        self.Omega.value[1][1] = 1.0

        self.Xi = matrix()
        self.Xi.zero(self.dim,1)
        self.Xi.value[0][0] = init_x
        self.Xi.value[1][0] = init_y

        self.landmarks = dict()
        self.num_landmarks = 0

        self.r = Robot(max_distance=1000,max_steering=PI)
        self.x = 0.0
        self.y = 0.0
        self.heading = 0.0
        self.measu_noise = measurement_noise
        self.mo_noise = motion_noise


    def slam_process_measurements(self, measurements):

        x = 0.0
        y = 0.0

        return x,y

    def online_slam_proces_measurements(self, measurements):
        """Process a new series of measurements.

        Args:
            measurements(dict): Collection of measurements
                in the format {'landmark id':{'distance':0.0, 'bearing':0.0, 'type':'beacon'}, ...}

        Returns:
            x, y: current belief in location of the rover relative to initial location before movement
        """
        measurement_noise = self.measu_noise
        #print "measurement_noise = ",measurement_noise

        for l in measurements:

            measurement_noise = self.measu_noise

            if l not in self.landmarks:
              #print l
              #self.landmarks[l] = self.num_landmarks
              #self.num_landmarks += 1
              #list = range(self.Omega.dimx)
              #dim = self.Omega.dimx+2
              #self.Omega = self.Omega.expand(dim, dim,list,list)
              #self.Xi = self.Xi.expand(dim, 1,list, [0])
               self.landmarks[l] = self.num_landmarks
               dim = 2*(1+self.num_landmarks+1)
               list = [0,1] + range(2,(self.num_landmarks+1)*2)
               self.Omega = self.Omega.expand(dim, dim,list,list)
               self.Xi = self.Xi.expand(dim, 1,list, [0])
               self.num_landmarks += 1

            reading =   measurements[l]
            d = reading['distance']
            b = reading['bearing']

            lx,ly = self.r.find_next_point(b,d)
            lx2 = lx-self.r.x
            ly2 = ly-self.r.y

            m = 2*(1+self.landmarks[l])
            xy = [lx2,ly2]

            if d > 0:
                measurement_noise = measurement_noise * d

            for b in range(2):
                self.Omega.value[b][b]   += 1.0 / measurement_noise
                self.Omega.value[m+b][m+b]   += 1.0 / measurement_noise
                self.Omega.value[b][m+b]   += -1.0 / measurement_noise
                self.Omega.value[m+b][b]   += -1.0 / measurement_noise
                self.Xi.value[b][0] += -xy[b] / measurement_noise
                self.Xi.value[m+b][0] += xy[b] / measurement_noise

        mu = self.Omega.inverse() * self.Xi

        x = mu[0][0]
        y = mu[1][0]

        self.x = x
        self.y = y

        return x, y

    def online_slam_process_motion(self, steering, distance):
        """Process a new movement.

         Args:
             steering(float): amount to turn
             distance(float): distance to move
             motion_noise(float): movement noise

         Returns:
             x, y: current belief in location of the rover relative to initial location after movement
         """

        motion_noise = self.mo_noise
        #print "motion_noise = ",motion_noise

        # TODO
        list = [0, 1] + range(4, self.Omega.dimx + 2)
        dim = self.Omega.dimx + 2
        self.Omega = self.Omega.expand(dim, dim, list, list)
        self.Xi = self.Xi.expand(dim, 1, list, [0])

        x1 = deepcopy(self.r.x)
        y1 = deepcopy(self.r.y)

        self.r.move(steering, distance)
        x2 = deepcopy(self.r.x)
        y2 = deepcopy(self.r.y)

        self.x = x2
        self.y = y2

        dx = x2 - x1
        dy = y2 - y1


        if dx < 0.01 and dx > -0.01:
            dx = 0.0
        if dy < 0.01 and dy > -0.01:
            dy = 0.0

        if(self.r.bearing < 0.01 and self.r.bearing > -0.01):
            self.r.bearing = 0.0

        if(self.r.bearing < ((pi/2)*1.1) and self.r.bearing > ((pi/2)*0.9)):
            self.r.bearing = pi/2

        if (self.r.bearing < ((pi) * 1.1) and self.r.bearing > ((pi) * 0.9)):
            self.r.bearing = pi

        if (self.r.bearing > ((-pi / 2) * 1.1) and self.r.bearing < ((-pi / 2) * 0.9)):
            self.r.bearing = -pi / 2

        #self.r.bearing = atan2(dy,dx)


        motion = [dx, dy]



        #motion_noise = motion_noise / (abs(distance+steering))

        for b in range(4):
            self.Omega.value[b][b] += 1.0 / motion_noise
        for b in range(2):
            self.Omega.value[b][b + 2] += -1.0 / motion_noise
            self.Omega.value[b + 2][b] += -1.0 / motion_noise
            self.Xi.value[b][0] += -motion[b] / motion_noise
            self.Xi.value[b + 2][0] += motion[b] / motion_noise

        newlist = range(2, len(self.Omega.value))
        a = self.Omega.take([0, 1], newlist)
        b = self.Omega.take([0, 1])
        c = self.Xi.take([0, 1], [0])
        self.Omega = self.Omega.take(newlist) - a.transpose() * b.inverse() * a
        self.Xi = self.Xi.take(newlist, [0]) - a.transpose() * b.inverse() * c

        mu = self.Omega.inverse() * self.Xi

        x = mu[0][0]
        y = mu[1][0]

        self.x = x
        self.y = y

        #if(steering != 0 and distance > 0.0):
        #    bearing_before = atan2(self.y-y1,self.x-x1)
        #    self.r.bearing = bearing_before
        #print "steering : ",steering
        #print "distance : ",distance



        return x,y

    def process_measurements(self, measurements):
        """Process a new series of measurements.

        Args:
            measurements(dict): Collection of measurements
                in the format {'landmark id':{'distance':0.0, 'bearing':0.0, 'type':'beacon'}, ...}

        Returns:
            x, y: current belief in location of the rover relative to initial location before movement
        """
        x,y = self.online_slam_proces_measurements(measurements)



        return x, y

    def process_movement(self, steering, distance, motion_noise=0.01):


        x,y = self.online_slam_process_motion(steering, distance)

        return x, y


class WayPointPlanner:
    """Create a planner to navigate the rover to reach all the intended way points from an unknown start position.
    """
    def __init__(self,  max_distance, max_steering):
        """Initialize your planner here.

        Args:
            max_distance(float): the max distance the robot can travel in a single move.
            max_steering(float): the max steering angle the robot can turn in a single move.
        """
        self.way_robot = Robot(max_distance=1.0, max_steering=PI/2.+0.01)
        #print "max_steering :",max_steering
        self.slam_state = SLAM()
        self.visitied = []
        self.todo = None
        self.current_waypoint = None
        self.current_waypoint_id = None
        self.localized = False
        self.spiral_state = 5
        self.spiral_state_counter = 5
        self.sampling = False
        self.search_point = None
        self.sample_loc = None

    def find_closest_site(self, todo):

        minDistance = 999999
        closestPoint = None

        for l in todo:
            d,b = self.way_robot.measure_distance_and_bearing_to(l)
            if d < minDistance:
                minDistance = d
                closestPoint = l

        #print "closest site to try for : ",closestPoint

        return closestPoint


    def find_closest_todo(self, list, measurements):

        minDistance = 999999
        closestPoint = None
        closestL = None

        for l in list:
            m = measurements[l]
            d = m['distance']
            if d < minDistance:
                minDistance = d
                closestPoint = m
                closestL = l

        return closestPoint, closestL

    def find_sample_sights(self, measurements):

        sample_sights = []

        for l in measurements:
            m = measurements[l]
            if m['type'] == "site":
                sample_sights.append(l)

        return sample_sights

    def update_current_waypoint(self,landmarks, measurements):

        for l in measurements:
            if l == self.current_waypoint_id:
                update = measurements[l]
                self.current_waypoint = update
                break #there is only ever one unique landmark

    def truncate_action(self, d,b):

        if b > self.way_robot.max_steering:
            b = self.way_robot.max_steering
        if d > self.way_robot.max_distance:
            d = self.way_robot.max_distance
        if b < -self.way_robot.max_steering:
            b = -self.way_robot.max_steering
        if d < -self.way_robot.max_distance:
            d = -self.way_robot.max_distance

        return d,b

    def next_move(self, sample_todo, measurements):
        """Next move based on the current set of measurements.

        Args:
            sample_todo(list): Set of locations remaining still needing a sample to be taken.
            measurements(dict): Collection of measurements from beacons and test sites in range.
                in the format {'landmark id':{'distance':0.0, 'bearing':0.0, 'type':'beacon'}, ...}

        Return:
            Next command to execute on the rover.
                allowed:
                    'move 1.570963 1.0' - turn left 90 degrees and move 1.0 distance
                    'sample' - take sample (will succeed if within tolerance of intended sample site)
        """

        #x,y = self.slam_state.process_measurements(measurements)
        b = 0.0
        d = 0.0
        sampled_point = None

        if self.sampling:
            if len(sample_todo) < len(self.todo):
                self.sampling = False
                self.current_waypoint = None
                self.current_waypoint_id = None

                sampled_point =  set(self.todo).symmetric_difference(set(sample_todo))
                self.way_robot.x = list(sampled_point)[0][0]
                self.way_robot.y = list(sampled_point)[0][1]
                #self.slam_state = SLAM(init_x=self.way_robot.x,init_y=self.way_robot.y)
                self.localized = True
            else:
                self.sampling = False
                self.localized = False
                #self.way_robot.x = (self.way_robot.x - self.sample_loc[0])
                #self.way_robot.y = (self.way_robot.y - self.sample_loc[1])
                #self.slam_state = SLAM(init_x=0.0, init_y=0.0)
        #else:
        #    self.way_robot.x = x
        #    self.way_robot.y = y


        sample_sites_l = self.find_sample_sights(measurements)


        print "---------------------------------------------------"
        print "sample_sites: ",sample_sites_l
        print "way point ",self.current_waypoint
        print "localized ",self.localized
        print "search site ",self.search_point
        print "self.todo : ",self.todo
        print "self.way_robot : " +str(self.way_robot.x) + " " + str(self.way_robot.y)


        if len(sample_sites_l) > 0:

            if self.current_waypoint == None:
                self.search_point = None
                self.current_waypoint, self.current_waypoint_id = self.find_closest_todo(sample_sites_l, measurements)
            else:
                result = self.update_current_waypoint(sample_sites_l, measurements)
                if result != None:
                    self.current_waypoint = result
                    #print " after updating waypoint : ",self.current_waypoint
                #else:
                    #print "didnt see the waypoint this cycle"

            d = self.current_waypoint['distance']
            b = self.current_waypoint['bearing']
            d,b = self.truncate_action(d,b)

            if d < 0.05:
                action = 'sample'
                self.sampling = True
                self.todo = deepcopy(sample_todo)
            else:
                action = 'move ' + str(b) + ' ' + str(d)
                #self.way_robot.move(b,d)

        elif self.localized == True:

            if self.current_waypoint != None:
                d = self.current_waypoint['distance']
                b = self.current_waypoint['bearing']
                d, b = self.truncate_action(d, b)


            #if self.search_point == None:
            self.search_point = self.find_closest_site(sample_todo)
            d,b = self.way_robot.measure_distance_and_bearing_to(self.search_point)
            d,b = self.truncate_action(d,b)


            if d < 0.05:
                action = 'sample'
                self.sampling = True
                self.todo = deepcopy(sample_todo)
                self.sample_loc = self.search_point
            else:
                action = 'move ' + str(b) + ' ' + str(d)
                #self.way_robot.move(b,d)
        else:
            if self.spiral_state_counter > 0:
                self.spiral_state_counter -= 1
            else:
                self.spiral_state += 1 
                self.spiral_state_counter = deepcopy(self.spiral_state)
                #self.localized = True
            action = 'move ' + str((2*PI/self.spiral_state)) + ' ' + str(self.way_robot.max_distance)
            #b = self.spiral_state
            b = (2*PI/self.spiral_state)
            d = self.way_robot.max_distance


        if action != 'sample':
            self.way_robot.move(b, d)
        #x, y = self.slam_state.process_movement(b, d)
        #self.way_robot.x = x
        #self.way_robot.y = y

        #print action

        return action




