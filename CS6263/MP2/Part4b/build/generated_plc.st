PROGRAM part4b
  VAR
    Tank1Low AT %IX0.0 : BOOL;
    Tank1Med AT %IX0.1 : BOOL;
    Tank1High AT %IX0.2 : BOOL;
    Tank2Low AT %IX0.3 : BOOL;
    Tank2Med AT %IX0.4 : BOOL;
    Tank2High AT %IX0.5 : BOOL;
    Tank3Low AT %IX0.6 : BOOL;
    Tank3Med AT %IX0.7 : BOOL;
    Tank3High AT %IX1.0 : BOOL;
    Tank4Low AT %IX1.1 : BOOL;
    Tank4Med AT %IX1.2 : BOOL;
    Tank4High AT %IX1.3 : BOOL;
    Tank1Out AT %QX0.0 : BOOL;
    Tank2Out AT %QX0.1 : BOOL;
    Tank3Out AT %QX0.2 : BOOL;
    Tank4Out AT %QX0.3 : BOOL;
    Bomb AT %QX0.4 : BOOL;
  END_VAR
  VAR
    TON0 : TON;
    NOT20_OUT : BOOL;
    AND19_OUT : BOOL;
  END_VAR

  Tank1Out := Tank1Med;
  NOT20_OUT := NOT(Bomb);
  AND19_OUT := AND(NOT20_OUT, Tank2Med);
  Tank2Out := AND19_OUT;
  Tank3Out := Tank3Med;
  Tank4Out := Tank4Med;
  TON0(IN := TRUE, PT := T#70S);
  Bomb := TON0.Q;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : part4b;
  END_RESOURCE
END_CONFIGURATION
