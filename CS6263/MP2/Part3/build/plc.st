TYPE
  LOGLEVEL : (CRITICAL, WARNING, INFO, DEBUG) := INFO;
END_TYPE

FUNCTION_BLOCK LOGGER
  VAR_INPUT
    TRIG : BOOL;
    MSG : STRING;
    LEVEL : LOGLEVEL := INFO;
  END_VAR
  VAR
    TRIG0 : BOOL;
  END_VAR

  IF TRIG AND NOT TRIG0 THEN
  {{
   LogMessage(GetFbVar(LEVEL),(char*)GetFbVar(MSG, .body),GetFbVar(MSG, .len));
  }}
  END_IF;
  TRIG0:=TRIG;
END_FUNCTION_BLOCK



FUNCTION_BLOCK python_eval
  VAR_INPUT
    TRIG : BOOL;
    CODE : STRING;
  END_VAR
  VAR_OUTPUT
    ACK : BOOL;
    RESULT : STRING;
  END_VAR
  VAR
    STATE : DWORD;
    BUFFER : STRING;
    PREBUFFER : STRING;
    TRIGM1 : BOOL;
    TRIGGED : BOOL;
  END_VAR

  {extern void __PythonEvalFB(int, PYTHON_EVAL*);__PythonEvalFB(0, data__);}
END_FUNCTION_BLOCK

FUNCTION_BLOCK python_poll
  VAR_INPUT
    TRIG : BOOL;
    CODE : STRING;
  END_VAR
  VAR_OUTPUT
    ACK : BOOL;
    RESULT : STRING;
  END_VAR
  VAR
    STATE : DWORD;
    BUFFER : STRING;
    PREBUFFER : STRING;
    TRIGM1 : BOOL;
    TRIGGED : BOOL;
  END_VAR

  {extern void __PythonEvalFB(int, PYTHON_EVAL*);__PythonEvalFB(1,(PYTHON_EVAL*)(void*)data__);}
END_FUNCTION_BLOCK

FUNCTION_BLOCK python_gear
  VAR_INPUT
    N : UINT;
    TRIG : BOOL;
    CODE : STRING;
  END_VAR
  VAR_OUTPUT
    ACK : BOOL;
    RESULT : STRING;
  END_VAR
  VAR
    py_eval : python_eval;
    COUNTER : UINT;
    ADD10_OUT : UINT;
    EQ13_OUT : BOOL;
    SEL15_OUT : UINT;
    AND7_OUT : BOOL;
  END_VAR

  ADD10_OUT := ADD(COUNTER, 1);
  EQ13_OUT := EQ(N, ADD10_OUT);
  SEL15_OUT := SEL(EQ13_OUT, ADD10_OUT, 0);
  COUNTER := SEL15_OUT;
  AND7_OUT := AND(EQ13_OUT, TRIG);
  py_eval(TRIG := AND7_OUT, CODE := CODE);
  ACK := py_eval.ACK;
  RESULT := py_eval.RESULT;
END_FUNCTION_BLOCK


PROGRAM part2
  VAR
    VehicleSensor : BOOL;
    PedCross : BOOL;
    VehRedL : BOOL;
    VehYelL : BOOL;
    VehGreL : BOOL;
    PedRedL : BOOL;
    PedGreL : BOOL;
  END_VAR
  VAR
    PedGoQ : BOOL;
    YellowQ : BOOL;
    R_TRIG0 : R_TRIG;
    TON0 : TON;
    SR0 : SR;
    SR1 : SR;
    R_TRIG1 : R_TRIG;
    TON1 : TON;
    SR2 : SR;
    F_TRIG0 : F_TRIG;
    NOT14_OUT : BOOL;
    AND35_OUT : BOOL;
    NOT31_OUT : BOOL;
    NOT10_OUT : BOOL;
    AND20_OUT : BOOL;
    NOT22_OUT : BOOL;
  END_VAR

  NOT14_OUT := NOT(VehicleSensor);
  R_TRIG0(CLK := PedCross);
  F_TRIG0(CLK := PedGoQ);
  SR2(S1 := R_TRIG0.Q, R := F_TRIG0.Q);
  AND35_OUT := AND(NOT14_OUT, SR2.Q1, VehGreL);
  TON0(IN := SR0.Q1, PT := T#3S);
  SR0(S1 := AND35_OUT, R := TON0.Q);
  YellowQ := SR0.Q1;
  R_TRIG1(CLK := TON0.Q);
  TON1(IN := SR1.Q1, PT := T#15S);
  SR1(S1 := R_TRIG1.Q, R := TON1.Q);
  PedGoQ := SR1.Q1;
  NOT31_OUT := NOT(YellowQ);
  NOT10_OUT := NOT(PedGoQ);
  AND20_OUT := AND(NOT31_OUT, NOT10_OUT);
  VehGreL := AND20_OUT;
  NOT22_OUT := NOT(PedGoQ);
  PedRedL := NOT22_OUT;
  VehYelL := YellowQ;
  PedGreL := PedGoQ;
  VehRedL := PedGoQ;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : part2;
  END_RESOURCE
END_CONFIGURATION
