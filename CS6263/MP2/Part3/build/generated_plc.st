PROGRAM part2
  VAR
    VehicleSensor AT %IX0.0 : BOOL;
    PedCross AT %IX0.1 : BOOL;
    VehRedL AT %QX0.0 : BOOL;
    VehYelL AT %QX0.1 : BOOL;
    VehGreL AT %QX0.2 : BOOL;
    PedRedL AT %QX0.3 : BOOL;
    PedGreL AT %QX0.4 : BOOL;
  END_VAR
  VAR
    PedGoQ : BOOL;
    YellowQ : BOOL;
    R_TRIG0 : R_TRIG;
    TON0 : TON;
    SR0 : SR;
    SR1 : SR;
    R_TRIG1 : R_TRIG;
    TON1 : TON;
    SR2 : SR;
    F_TRIG0 : F_TRIG;
    NOT14_OUT : BOOL;
    AND35_OUT : BOOL;
    NOT31_OUT : BOOL;
    NOT10_OUT : BOOL;
    AND20_OUT : BOOL;
    NOT22_OUT : BOOL;
  END_VAR

  NOT14_OUT := NOT(VehicleSensor);
  R_TRIG0(CLK := PedCross);
  F_TRIG0(CLK := PedGoQ);
  SR2(S1 := R_TRIG0.Q, R := F_TRIG0.Q);
  AND35_OUT := AND(NOT14_OUT, SR2.Q1, VehGreL);
  TON0(IN := SR0.Q1, PT := T#3S);
  SR0(S1 := AND35_OUT, R := TON0.Q);
  YellowQ := SR0.Q1;
  R_TRIG1(CLK := TON0.Q);
  TON1(IN := SR1.Q1, PT := T#15S);
  SR1(S1 := R_TRIG1.Q, R := TON1.Q);
  PedGoQ := SR1.Q1;
  NOT31_OUT := NOT(YellowQ);
  NOT10_OUT := NOT(PedGoQ);
  AND20_OUT := AND(NOT31_OUT, NOT10_OUT);
  VehGreL := AND20_OUT;
  NOT22_OUT := NOT(PedGoQ);
  PedRedL := NOT22_OUT;
  VehYelL := YellowQ;
  PedGreL := PedGoQ;
  VehRedL := PedGoQ;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : part2;
  END_RESOURCE
END_CONFIGURATION
