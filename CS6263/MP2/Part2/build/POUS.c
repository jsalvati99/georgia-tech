void LOGGER_init__(LOGGER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->TRIG,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->MSG,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->LEVEL,LOGLEVEL__INFO,retain)
  __INIT_VAR(data__->TRIG0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void LOGGER_body__(LOGGER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->TRIG,) && !(__GET_VAR(data__->TRIG0,)))) {
    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,__VA_ARGS__,val)

   LogMessage(GetFbVar(LEVEL),(char*)GetFbVar(MSG, .body),GetFbVar(MSG, .len));
  
    #undef GetFbVar
    #undef SetFbVar
;
  };
  __SET_VAR(data__->,TRIG0,,__GET_VAR(data__->TRIG,));

  goto __end;

__end:
  return;
} // LOGGER_body__() 





void PYTHON_EVAL_init__(PYTHON_EVAL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->TRIG,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CODE,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->RESULT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->STATE,0,retain)
  __INIT_VAR(data__->BUFFER,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->PREBUFFER,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->TRIGM1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TRIGGED,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void PYTHON_EVAL_body__(PYTHON_EVAL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,__VA_ARGS__,val)
extern void __PythonEvalFB(int, PYTHON_EVAL*);__PythonEvalFB(0, data__);
  #undef GetFbVar
  #undef SetFbVar
;

  goto __end;

__end:
  return;
} // PYTHON_EVAL_body__() 





void PYTHON_POLL_init__(PYTHON_POLL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->TRIG,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CODE,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->RESULT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->STATE,0,retain)
  __INIT_VAR(data__->BUFFER,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->PREBUFFER,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->TRIGM1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TRIGGED,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void PYTHON_POLL_body__(PYTHON_POLL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,__VA_ARGS__,val)
extern void __PythonEvalFB(int, PYTHON_EVAL*);__PythonEvalFB(1,(PYTHON_EVAL*)(void*)data__);
  #undef GetFbVar
  #undef SetFbVar
;

  goto __end;

__end:
  return;
} // PYTHON_POLL_body__() 





void PYTHON_GEAR_init__(PYTHON_GEAR *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->N,0,retain)
  __INIT_VAR(data__->TRIG,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CODE,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->RESULT,__STRING_LITERAL(0,""),retain)
  PYTHON_EVAL_init__(&data__->PY_EVAL,retain);
  __INIT_VAR(data__->COUNTER,0,retain)
  __INIT_VAR(data__->ADD10_OUT,0,retain)
  __INIT_VAR(data__->EQ13_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SEL15_OUT,0,retain)
  __INIT_VAR(data__->AND7_OUT,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void PYTHON_GEAR_body__(PYTHON_GEAR *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,ADD10_OUT,,ADD__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->COUNTER,),
    (UINT)1));
  __SET_VAR(data__->,EQ13_OUT,,EQ__BOOL__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->N,),
    (UINT)__GET_VAR(data__->ADD10_OUT,)));
  __SET_VAR(data__->,SEL15_OUT,,SEL__UINT__BOOL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->EQ13_OUT,),
    (UINT)__GET_VAR(data__->ADD10_OUT,),
    (UINT)0));
  __SET_VAR(data__->,COUNTER,,__GET_VAR(data__->SEL15_OUT,));
  __SET_VAR(data__->,AND7_OUT,,AND__BOOL__BOOL(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->EQ13_OUT,),
    (BOOL)__GET_VAR(data__->TRIG,)));
  __SET_VAR(data__->PY_EVAL.,TRIG,,__GET_VAR(data__->AND7_OUT,));
  __SET_VAR(data__->PY_EVAL.,CODE,,__GET_VAR(data__->CODE,));
  PYTHON_EVAL_body__(&data__->PY_EVAL);
  __SET_VAR(data__->,ACK,,__GET_VAR(data__->PY_EVAL.ACK,));
  __SET_VAR(data__->,RESULT,,__GET_VAR(data__->PY_EVAL.RESULT,));

  goto __end;

__end:
  return;
} // PYTHON_GEAR_body__() 





void PART2_init__(PART2 *data__, BOOL retain) {
  __INIT_VAR(data__->POSX0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSX1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSX2,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSY0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSY1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSY2,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TARX0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TARX1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TARX2,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TARY0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TARY1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TARY2,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GOUP,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GODOWN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GOLEFT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GORIGHT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GORIGHTQ,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GOLEFTQ,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GOUPQ,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GODOWNQ,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSX,0,retain)
  __INIT_VAR(data__->POSY,0,retain)
  __INIT_VAR(data__->TARX,0,retain)
  __INIT_VAR(data__->TARY,0,retain)
  __INIT_VAR(data__->NOT52_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->AND51_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->NOT55_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->AND54_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->NOT58_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->AND57_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GT5_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GT28_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LT1_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LT8_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BOOL_TO_UINT69_OUT,0,retain)
  __INIT_VAR(data__->MUL108_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT91_OUT,0,retain)
  __INIT_VAR(data__->MUL106_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT93_OUT,0,retain)
  __INIT_VAR(data__->MUL103_OUT,0,retain)
  __INIT_VAR(data__->ADD109_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT117_OUT,0,retain)
  __INIT_VAR(data__->MUL135_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT119_OUT,0,retain)
  __INIT_VAR(data__->MUL133_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT121_OUT,0,retain)
  __INIT_VAR(data__->MUL130_OUT,0,retain)
  __INIT_VAR(data__->ADD136_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT90_OUT,0,retain)
  __INIT_VAR(data__->MUL96_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT92_OUT,0,retain)
  __INIT_VAR(data__->MUL98_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT94_OUT,0,retain)
  __INIT_VAR(data__->MUL100_OUT,0,retain)
  __INIT_VAR(data__->ADD95_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT118_OUT,0,retain)
  __INIT_VAR(data__->MUL124_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT120_OUT,0,retain)
  __INIT_VAR(data__->MUL126_OUT,0,retain)
  __INIT_VAR(data__->BOOL_TO_UINT122_OUT,0,retain)
  __INIT_VAR(data__->MUL128_OUT,0,retain)
  __INIT_VAR(data__->ADD123_OUT,0,retain)
}

// Code part
void PART2_body__(PART2 *data__) {
  // Initialise TEMP variables

  __SET_VAR(data__->,GORIGHT,,__GET_VAR(data__->GORIGHTQ,));
  __SET_VAR(data__->,NOT52_OUT,,!(__GET_VAR(data__->GORIGHTQ,)));
  __SET_VAR(data__->,AND51_OUT,,AND__BOOL__BOOL(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->NOT52_OUT,),
    (BOOL)__GET_VAR(data__->GOLEFTQ,)));
  __SET_VAR(data__->,GOLEFT,,__GET_VAR(data__->AND51_OUT,));
  __SET_VAR(data__->,NOT55_OUT,,!(__GET_VAR(data__->GOLEFTQ,)));
  __SET_VAR(data__->,AND54_OUT,,AND__BOOL__BOOL(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)3,
    (BOOL)__GET_VAR(data__->NOT52_OUT,),
    (BOOL)__GET_VAR(data__->NOT55_OUT,),
    (BOOL)__GET_VAR(data__->GOUPQ,)));
  __SET_VAR(data__->,GOUP,,__GET_VAR(data__->AND54_OUT,));
  __SET_VAR(data__->,NOT58_OUT,,!(__GET_VAR(data__->GOUPQ,)));
  __SET_VAR(data__->,AND57_OUT,,AND__BOOL__BOOL(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)4,
    (BOOL)__GET_VAR(data__->NOT52_OUT,),
    (BOOL)__GET_VAR(data__->NOT55_OUT,),
    (BOOL)__GET_VAR(data__->NOT58_OUT,),
    (BOOL)__GET_VAR(data__->GODOWNQ,)));
  __SET_VAR(data__->,GODOWN,,__GET_VAR(data__->AND57_OUT,));
  __SET_VAR(data__->,GT5_OUT,,GT__BOOL__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->TARX,),
    (UINT)__GET_VAR(data__->POSX,)));
  __SET_VAR(data__->,GORIGHTQ,,__GET_VAR(data__->GT5_OUT,));
  __SET_VAR(data__->,GT28_OUT,,GT__BOOL__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->TARY,),
    (UINT)__GET_VAR(data__->POSY,)));
  __SET_VAR(data__->,GOUPQ,,__GET_VAR(data__->GT28_OUT,));
  __SET_VAR(data__->,LT1_OUT,,LT__BOOL__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->TARX,),
    (UINT)__GET_VAR(data__->POSX,)));
  __SET_VAR(data__->,GOLEFTQ,,__GET_VAR(data__->LT1_OUT,));
  __SET_VAR(data__->,LT8_OUT,,LT__BOOL__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->TARY,),
    (UINT)__GET_VAR(data__->POSY,)));
  __SET_VAR(data__->,GODOWNQ,,__GET_VAR(data__->LT8_OUT,));
  __SET_VAR(data__->,BOOL_TO_UINT69_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->TARX2,)));
  __SET_VAR(data__->,MUL108_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT69_OUT,),
    (UINT)4));
  __SET_VAR(data__->,BOOL_TO_UINT91_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)((__GET_VAR(data__->TARX1,) || __GET_VAR(data__->TARX1,)) || __GET_VAR(data__->TARX1,))));
  __SET_VAR(data__->,MUL106_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT91_OUT,),
    (UINT)2));
  __SET_VAR(data__->,BOOL_TO_UINT93_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->TARX0,)));
  __SET_VAR(data__->,MUL103_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT93_OUT,),
    (UINT)1));
  __SET_VAR(data__->,ADD109_OUT,,ADD__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)3,
    (UINT)__GET_VAR(data__->MUL108_OUT,),
    (UINT)__GET_VAR(data__->MUL106_OUT,),
    (UINT)__GET_VAR(data__->MUL103_OUT,)));
  __SET_VAR(data__->,TARX,,__GET_VAR(data__->ADD109_OUT,));
  __SET_VAR(data__->,BOOL_TO_UINT117_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->TARY2,)));
  __SET_VAR(data__->,MUL135_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT117_OUT,),
    (UINT)4));
  __SET_VAR(data__->,BOOL_TO_UINT119_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)((__GET_VAR(data__->TARY1,) || __GET_VAR(data__->TARY1,)) || __GET_VAR(data__->TARY1,))));
  __SET_VAR(data__->,MUL133_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT119_OUT,),
    (UINT)2));
  __SET_VAR(data__->,BOOL_TO_UINT121_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->TARY0,)));
  __SET_VAR(data__->,MUL130_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT121_OUT,),
    (UINT)1));
  __SET_VAR(data__->,ADD136_OUT,,ADD__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)3,
    (UINT)__GET_VAR(data__->MUL135_OUT,),
    (UINT)__GET_VAR(data__->MUL133_OUT,),
    (UINT)__GET_VAR(data__->MUL130_OUT,)));
  __SET_VAR(data__->,TARY,,__GET_VAR(data__->ADD136_OUT,));
  __SET_VAR(data__->,BOOL_TO_UINT90_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->POSX2,)));
  __SET_VAR(data__->,MUL96_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT90_OUT,),
    (UINT)4));
  __SET_VAR(data__->,BOOL_TO_UINT92_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->POSX1,)));
  __SET_VAR(data__->,MUL98_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT92_OUT,),
    (UINT)2));
  __SET_VAR(data__->,BOOL_TO_UINT94_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->POSX0,)));
  __SET_VAR(data__->,MUL100_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT94_OUT,),
    (UINT)1));
  __SET_VAR(data__->,ADD95_OUT,,ADD__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)3,
    (UINT)__GET_VAR(data__->MUL96_OUT,),
    (UINT)__GET_VAR(data__->MUL98_OUT,),
    (UINT)__GET_VAR(data__->MUL100_OUT,)));
  __SET_VAR(data__->,POSX,,__GET_VAR(data__->ADD95_OUT,));
  __SET_VAR(data__->,BOOL_TO_UINT118_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->POSY2,)));
  __SET_VAR(data__->,MUL124_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT118_OUT,),
    (UINT)4));
  __SET_VAR(data__->,BOOL_TO_UINT120_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->POSY1,)));
  __SET_VAR(data__->,MUL126_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT120_OUT,),
    (UINT)2));
  __SET_VAR(data__->,BOOL_TO_UINT122_OUT,,BOOL_TO_UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->POSY0,)));
  __SET_VAR(data__->,MUL128_OUT,,MUL__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UINT)__GET_VAR(data__->BOOL_TO_UINT122_OUT,),
    (UINT)1));
  __SET_VAR(data__->,ADD123_OUT,,ADD__UINT__UINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)3,
    (UINT)__GET_VAR(data__->MUL124_OUT,),
    (UINT)__GET_VAR(data__->MUL126_OUT,),
    (UINT)__GET_VAR(data__->MUL128_OUT,)));
  __SET_VAR(data__->,POSY,,__GET_VAR(data__->ADD123_OUT,));

  goto __end;

__end:
  return;
} // PART2_body__() 





