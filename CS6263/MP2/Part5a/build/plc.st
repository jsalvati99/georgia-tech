TYPE
  LOGLEVEL : (CRITICAL, WARNING, INFO, DEBUG) := INFO;
END_TYPE

FUNCTION_BLOCK LOGGER
  VAR_INPUT
    TRIG : BOOL;
    MSG : STRING;
    LEVEL : LOGLEVEL := INFO;
  END_VAR
  VAR
    TRIG0 : BOOL;
  END_VAR

  IF TRIG AND NOT TRIG0 THEN
  {{
   LogMessage(GetFbVar(LEVEL),(char*)GetFbVar(MSG, .body),GetFbVar(MSG, .len));
  }}
  END_IF;
  TRIG0:=TRIG;
END_FUNCTION_BLOCK



FUNCTION_BLOCK python_eval
  VAR_INPUT
    TRIG : BOOL;
    CODE : STRING;
  END_VAR
  VAR_OUTPUT
    ACK : BOOL;
    RESULT : STRING;
  END_VAR
  VAR
    STATE : DWORD;
    BUFFER : STRING;
    PREBUFFER : STRING;
    TRIGM1 : BOOL;
    TRIGGED : BOOL;
  END_VAR

  {extern void __PythonEvalFB(int, PYTHON_EVAL*);__PythonEvalFB(0, data__);}
END_FUNCTION_BLOCK

FUNCTION_BLOCK python_poll
  VAR_INPUT
    TRIG : BOOL;
    CODE : STRING;
  END_VAR
  VAR_OUTPUT
    ACK : BOOL;
    RESULT : STRING;
  END_VAR
  VAR
    STATE : DWORD;
    BUFFER : STRING;
    PREBUFFER : STRING;
    TRIGM1 : BOOL;
    TRIGGED : BOOL;
  END_VAR

  {extern void __PythonEvalFB(int, PYTHON_EVAL*);__PythonEvalFB(1,(PYTHON_EVAL*)(void*)data__);}
END_FUNCTION_BLOCK

FUNCTION_BLOCK python_gear
  VAR_INPUT
    N : UINT;
    TRIG : BOOL;
    CODE : STRING;
  END_VAR
  VAR_OUTPUT
    ACK : BOOL;
    RESULT : STRING;
  END_VAR
  VAR
    py_eval : python_eval;
    COUNTER : UINT;
    ADD10_OUT : UINT;
    EQ13_OUT : BOOL;
    SEL15_OUT : UINT;
    AND7_OUT : BOOL;
  END_VAR

  ADD10_OUT := ADD(COUNTER, 1);
  EQ13_OUT := EQ(N, ADD10_OUT);
  SEL15_OUT := SEL(EQ13_OUT, ADD10_OUT, 0);
  COUNTER := SEL15_OUT;
  AND7_OUT := AND(EQ13_OUT, TRIG);
  py_eval(TRIG := AND7_OUT, CODE := CODE);
  ACK := py_eval.ACK;
  RESULT := py_eval.RESULT;
END_FUNCTION_BLOCK


PROGRAM part5
  VAR
    Empty : BOOL;
    LowL : BOOL;
    MediumL : BOOL;
    HighL : BOOL;
    InMat1A : BOOL;
    InMat2B : BOOL;
    InMat3C : BOOL;
    Stirring : BOOL;
    Outlet : BOOL;
    Emptying : BOOL;
  END_VAR
  VAR
    SR0 : SR;
    SR1 : SR;
    R_TRIG0 : R_TRIG;
    R_TRIG1 : R_TRIG;
    SR2 : SR;
    SR3 : SR;
    R_TRIG2 : R_TRIG;
    TON0 : TON;
    SR4 : SR;
    SR5 : SR;
    TON1 : TON;
    SR6 : SR;
    NOT28_OUT : BOOL;
    NOT24_OUT : BOOL;
    AND29_OUT : BOOL;
    OR26_OUT : BOOL;
    NOT13_OUT : BOOL;
    OR32_OUT : BOOL;
    AND36_OUT : BOOL;
    OR34_OUT : BOOL;
    OR31_OUT : BOOL;
  END_VAR

  NOT28_OUT := NOT(Emptying);
  NOT24_OUT := NOT(Empty);
  AND29_OUT := AND(NOT28_OUT, NOT24_OUT);
  SR0(S1 := AND29_OUT, R := LowL);
  InMat1A := SR0.Q1;
  R_TRIG0(CLK := LowL);
  SR1(S1 := R_TRIG0.Q, R := MediumL);
  R_TRIG1(CLK := MediumL);
  SR2(S1 := R_TRIG1.Q, R := HighL);
  OR26_OUT := OR(SR0.Q1, SR1.Q1, SR2.Q1);
  InMat2B := OR26_OUT;
  R_TRIG2(CLK := HighL);
  TON0(IN := SR3.Q1, PT := T#5S);
  SR3(S1 := R_TRIG2.Q, R := TON0.Q);
  NOT13_OUT := NOT(Empty);
  SR4(S1 := TON0.Q, R := NOT13_OUT);
  OR32_OUT := OR(SR3.Q1, SR4.Q1);
  Stirring := OR32_OUT;
  InMat3C := SR2.Q1;
  TON1(IN := SR5.Q1, PT := T#3S);
  SR6(S1 := SR4.Q1, R := TON1.Q);
  AND36_OUT := AND(SR6.Q1, NOT13_OUT);
  SR5(S1 := AND36_OUT, R := TON1.Q);
  OR34_OUT := OR(SR4.Q1, SR5.Q1);
  Outlet := OR34_OUT;
  OR31_OUT := OR(SR4.Q1, SR5.Q1);
  Emptying := OR31_OUT;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : part5;
  END_RESOURCE
END_CONFIGURATION
