PROGRAM part1a
  VAR
    Input0 AT %IX0.0 : BOOL;
    Input1 AT %IX0.1 : BOOL;
    Output0 AT %QX0.0 : BOOL;
    Output1 AT %QX0.1 : BOOL;
  END_VAR
  VAR
    NOT8_OUT : BOOL;
    AND7_OUT : BOOL;
  END_VAR

  NOT8_OUT := NOT(Input1);
  AND7_OUT := AND(Input0, NOT8_OUT);
  Output0 := AND7_OUT;
  Output1 := Input1;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : part1a;
  END_RESOURCE
END_CONFIGURATION
