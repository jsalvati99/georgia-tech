PROGRAM part5b
  VAR
    Empty AT %IX0.0 : BOOL;
    LowL AT %IX0.1 : BOOL;
    MediumL AT %IX0.2 : BOOL;
    HighL AT %IX0.3 : BOOL;
    InMat1A AT %QX0.0 : BOOL;
    InMat2B AT %QX0.1 : BOOL;
    InMat3C AT %QX0.2 : BOOL;
    Stirring AT %QX0.3 : BOOL;
    Outlet AT %QX0.4 : BOOL;
    Emptying AT %QX0.5 : BOOL;
    Bomb AT %QX0.6 : BOOL;
  END_VAR
  VAR
    SR0 : SR;
    SR1 : SR;
    R_TRIG0 : R_TRIG;
    R_TRIG1 : R_TRIG;
    SR2 : SR;
    SR3 : SR;
    R_TRIG2 : R_TRIG;
    TON0 : TON;
    SR4 : SR;
    SR5 : SR;
    TON1 : TON;
    SR6 : SR;
    CTU0 : CTU;
    R_TRIG3 : R_TRIG;
    NOT28_OUT : BOOL;
    NOT24_OUT : BOOL;
    AND29_OUT : BOOL;
    R_TRIG4 : R_TRIG;
    OR26_OUT : BOOL;
    NOT13_OUT : BOOL;
    OR32_OUT : BOOL;
    XOR48_OUT : BOOL;
    AND36_OUT : BOOL;
    OR34_OUT : BOOL;
    OR31_OUT : BOOL;
  END_VAR

  NOT28_OUT := NOT(Emptying);
  NOT24_OUT := NOT(Empty);
  AND29_OUT := AND(NOT28_OUT, NOT24_OUT);
  SR0(S1 := AND29_OUT, R := LowL);
  InMat1A := SR0.Q1;
  R_TRIG3(CLK := SR0.Q1);
  R_TRIG4(CLK := R_TRIG3.Q);
  CTU0(CU := R_TRIG4.Q, PV := 3);
  Bomb := CTU0.Q;
  R_TRIG0(CLK := LowL);
  SR1(S1 := R_TRIG0.Q, R := MediumL);
  R_TRIG1(CLK := MediumL);
  SR2(S1 := R_TRIG1.Q, R := HighL);
  OR26_OUT := OR(SR0.Q1, SR1.Q1, SR2.Q1);
  InMat2B := OR26_OUT;
  R_TRIG2(CLK := HighL);
  TON0(IN := SR3.Q1, PT := T#5S);
  SR3(S1 := R_TRIG2.Q, R := TON0.Q);
  NOT13_OUT := NOT(Empty);
  SR4(S1 := TON0.Q, R := NOT13_OUT);
  OR32_OUT := OR(SR3.Q1, SR4.Q1);
  XOR48_OUT := XOR(Bomb, OR32_OUT);
  Stirring := XOR48_OUT;
  InMat3C := SR2.Q1;
  TON1(IN := SR5.Q1, PT := T#3S);
  SR6(S1 := SR4.Q1, R := TON1.Q);
  AND36_OUT := AND(SR6.Q1, NOT13_OUT);
  SR5(S1 := AND36_OUT, R := TON1.Q);
  OR34_OUT := OR(SR4.Q1, SR5.Q1);
  Outlet := OR34_OUT;
  OR31_OUT := OR(SR4.Q1, SR5.Q1);
  Emptying := OR31_OUT;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : part5b;
  END_RESOURCE
END_CONFIGURATION
