PROGRAM part4a
  VAR
    Tank1Low AT %IX0.0 : BOOL;
    Tank1Med AT %IX0.1 : BOOL;
    Tank1High AT %IX0.2 : BOOL;
    Tank2Low AT %IX0.3 : BOOL;
    Tank2Med AT %IX0.4 : BOOL;
    Tank2High AT %IX0.5 : BOOL;
    Tank3Low AT %IX0.6 : BOOL;
    Tank3Med AT %IX0.7 : BOOL;
    Tank3High AT %IX1.0 : BOOL;
    Tank4Low AT %IX1.1 : BOOL;
    Tank4Med AT %IX1.2 : BOOL;
    Tank4High AT %IX1.3 : BOOL;
    Tank1Out AT %QX0.0 : BOOL;
    Tank2Out AT %QX0.1 : BOOL;
    Tank3Out AT %QX0.2 : BOOL;
    Tank4Out AT %QX0.3 : BOOL;
  END_VAR

  Tank1Out := Tank1Med;
  Tank2Out := Tank2Med;
  Tank3Out := Tank3Med;
  Tank4Out := Tank4Med;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : part4a;
  END_RESOURCE
END_CONFIGURATION
